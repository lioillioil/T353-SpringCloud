package com.bdqn.controller;

import com.bdqn.base.R;
import com.bdqn.constant.PayTypeEnum;
import com.bdqn.logger.WebLogger;
import com.bdqn.pojo.Payment;
import com.bdqn.service.PaymentService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * PaymentController 支付接口
 *
 * @author LILIBO
 * @since 2024-03-19
 */
@RestController
@RequestMapping("/pay")
public class PaymentController {

    @Value("${server.port}")
    private String serverPort;

    /**
     * 引入业务逻辑层
     */
    @Resource
    private PaymentService paymentService;

    /**
     * 根据订单编号完成支付
     */
    @WebLogger("根据订单编号完成支付")
    @PostMapping("/byOrderId")
    public R payByOrderId(@RequestParam("orderId") String orderId, @RequestParam("money") Double money) {
        Payment payment = paymentService.sendPay(orderId, money, PayTypeEnum.WeChatPay.getVal());
        return R.success("【支付模块 - " + serverPort + "】订单: " + orderId + " （金额: " + money + "）支付成功！", payment);
    }

}
