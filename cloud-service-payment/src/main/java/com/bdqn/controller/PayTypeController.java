package com.bdqn.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.base.Q;
import com.bdqn.base.R;
import com.bdqn.logger.WebLogger;
import com.bdqn.pojo.PayType;
import com.bdqn.service.PayTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 支付
 *
 * @author LILIBO
 * @since 2022-06-24
 */
@Api(tags = "支付接口")
@RestController
@RequestMapping("/paytype")
public class PayTypeController {

    @Resource
    private PayTypeService payTypeService;

    /**
     * 新增支付记录
     *
     * @param payType 支付
     * @return R 成功返回true
     */
    @ApiOperation("新增支付记录")
    @WebLogger("新增支付记录")
    @PostMapping("/add")
    public R add(@ApiParam("支付对象") @RequestBody PayType payType) {
        return R.success(payTypeService.save(payType));
    }

    /**
     * 保存或更新支付记录
     *
     * @param payType 支付（带主键ID表示更新，否则为新增）
     * @return R 成功返回保存后的完整对象
     */
    @ApiOperation("保存或更新支付记录")
    @WebLogger("保存或更新支付记录")
    @PostMapping("/save")
    public R save(@ApiParam("支付对象") @RequestBody PayType payType) {
        return payTypeService.saveOrUpdate(payType) ? R.success(payType) : R.failure();
    }

    /**
     * 修改支付记录
     *
     * @param payType 支付
     * @return R 成功返回true
     */
    @ApiOperation("修改支付记录")
    @WebLogger("修改支付记录")
    @PostMapping("/update")
    public R update(@ApiParam("支付对象") @RequestBody PayType payType) {
        return R.success(payTypeService.updateById(payType));
    }

    /**
     * 通过id删除一条支付记录
     *
     * @param id
     * @return R 成功返回true
     */
    @ApiOperation("通过id删除一条支付记录")
    @WebLogger("通过id删除一条支付记录")
    @PostMapping("/delete")
    public R deleteById(@ApiParam("支付id") Integer id) {
        return R.success(payTypeService.removeById(id));
    }

    /**
     * 通过多个id批量删除支付记录
     *
     * @param ids
     * @return R
     */
    @ApiOperation("通过多个id批量删除支付记录")
    @WebLogger("通过多个id批量删除支付记录")
    @PostMapping("/remove")
    public R removeByIds(@ApiParam("支付id集合") @RequestBody List<Integer> ids) {
        return R.success(payTypeService.removeByIds(ids));
    }

    /**
     * 通过id查询单条支付记录
     *
     * @param id
     * @return R
     */
    @ApiOperation("通过id查询单条支付记录")
    @WebLogger("通过id查询单条支付记录")
    @GetMapping("/{id}")
    public R<PayType> getById(@ApiParam("支付id") @PathVariable Integer id) {
        return R.success(payTypeService.getById(id));
    }

    /**
     * 查询支付列表
     *
     * @param payType 支付
     * @return R
     */
    @ApiOperation("查询支付列表")
    @WebLogger("查询支付列表")
    @GetMapping("/list")
    public R<List<PayType>> getList(@ApiParam("查询的支付对象") PayType payType) {
        return R.success(payTypeService.getList(payType));
    }

    /**
     * 分页查询支付
     *
     * @param pageNo 当前页
     * @param pageSize 每页条数
     * @param payType 支付
     * @return
     */
    @ApiOperation("分页查询支付")
    @WebLogger("分页查询支付")
    @GetMapping("/page")
    public R<IPage<PayType>> getPage(@ApiParam("当前页") @RequestParam(required = false, defaultValue = "1") Long pageNo, @ApiParam("每页条数") @RequestParam(required = false, defaultValue = "10") Long pageSize, @ApiParam("查询的支付对象") PayType payType) {
        return R.success(payTypeService.getPage(new Page<>(pageNo, pageSize), payType));
    }

    /**
     * 分页查询支付列表
     *
     * @param pageNo 当前页
     * @param pageSize 每页条数
     * @param param 通用查询参数
     * @return 返回分页数据列表，其中count为数据总量
     */
    @ApiOperation("分页查询支付列表")
    @WebLogger("分页查询支付列表")
    @GetMapping("/pagelist")
    public R<List<PayType>> getPageList(@ApiParam("当前页") @RequestParam(required = false, defaultValue = "1") Long pageNo, @ApiParam("每页条数") @RequestParam(required = false, defaultValue = "10") Long pageSize, @ApiParam("通用查询参数") Q param) {
        IPage<PayType> page = payTypeService.getPageList(new Page<>(pageNo, pageSize), param);
        return R.success(page.getTotal(), page.getRecords());
    }

}
