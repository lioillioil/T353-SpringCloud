package com.bdqn.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.base.Q;
import com.bdqn.mapper.PayTypeMapper;
import com.bdqn.pojo.PayType;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 支付
 *
 * @author LILIBO
 * @since 2022-06-24
 */
@Service
public class PayTypeServiceImpl extends ServiceImpl<PayTypeMapper, PayType> implements PayTypeService {

    @Resource
    private PayTypeMapper payTypeMapper;

    /**
     * 获取支付分页数据
     *
     * @param page
     * @param param
     * @return
     */
    @Override
    public IPage<PayType> getPageList(Page page, Q param) {
        return payTypeMapper.getPageList(page, param);
    }

}
