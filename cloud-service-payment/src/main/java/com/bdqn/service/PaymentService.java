package com.bdqn.service;

import com.bdqn.pojo.Payment;

/**
 * PaymentService
 *
 * @author LILIBO
 * @since 2024-03-19
 */
public interface PaymentService extends IService<Payment> {

    /**
     * 发起支付
     *
     * @param orderId 订单ID
     * @param money 订单金额
     * @param payType 支付类型
     * @return
     */
    Payment sendPay(String orderId, Double money, int payType);

}
