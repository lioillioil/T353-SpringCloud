package com.bdqn.service;

import com.bdqn.constant.PayTypeEnum;
import com.bdqn.mapper.PaymentMapper;
import com.bdqn.pojo.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * PaymentServiceImpl
 *
 * @author LILIBO
 * @since 2024-03-19
 */
@Slf4j
@Service
public class PaymentServiceImpl extends ServiceImpl<PaymentMapper, Payment> implements PaymentService {

    /**
     * 引入数据访问层
     */
    @Resource
    private PaymentMapper paymentMapper;

    /**
     * 发起支付
     *
     * @param orderId 订单ID
     * @param money 订单金额
     * @param payType 支付类型
     * @return
     */
    @Override
    public Payment sendPay(String orderId, Double money, int payType) {
        log.info("支付方式：" + PayTypeEnum.get(payType) + "（支付成功）");
        String nowTime = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now());
        Payment payment = new Payment(orderId, payType, nowTime, money, "支付成功", 1L, 1L, nowTime, nowTime);
        // payment.setId(SnowFlakeUtil.getNextId()); // 雪花算法生成ID
        int result = paymentMapper.insert(payment);
        log.debug("支付记录插入" + (result == 1 ? "成功" : "失败"));
        return payment;
    }

}
