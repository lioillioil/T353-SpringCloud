package com.bdqn.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.base.Q;
import com.bdqn.pojo.PayType;

/**
 * 支付
 *
 * @author LILIBO
 * @since 2022-06-24
 */
public interface PayTypeService extends IService<PayType> {

    /**
     * 获取支付分页数据
     *
     * @param page
     * @param param
     * @return
     */
    IPage<PayType> getPageList(Page page, Q param);

}
