package com.bdqn.mapper;

import com.bdqn.pojo.Payment;

/**
 * PaymentMapper 支付数据访问层接口
 *
 * @author LILIBO
 * @since 2024-03-19
 */
public interface PaymentMapper extends IMapper<Payment> {
}
