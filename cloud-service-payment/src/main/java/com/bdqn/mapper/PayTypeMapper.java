package com.bdqn.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.base.Q;
import com.bdqn.pojo.PayType;
import org.apache.ibatis.annotations.Param;

/**
 * 支付
 *
 * @author LILIBO
 * @since 2022-06-24
 */
public interface PayTypeMapper extends IMapper<PayType> {

    /**
     * 获取支付分页数据
     *
     * @param page
     * @param param
     * @return
     */
    IPage<PayType> getPageList(Page page, @Param("q") Q param);

}
