package com.bdqn.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.base.Q;
import com.bdqn.pojo.Seckill;

/**
 * 秒杀活动
 *
 * @author LILIBO
 * @since 2022-06-24
 */
public interface SeckillService extends IService<Seckill> {

    /**
     * 获取秒杀活动分页数据
     *
     * @param page
     * @param param
     * @return
     */
    IPage<Seckill> getPageList(Page page, Q param);

    /**
     * 商品秒杀
     *
     * @param seckillId
     * @return
     */
    long viebuy(Long seckillId);

}
