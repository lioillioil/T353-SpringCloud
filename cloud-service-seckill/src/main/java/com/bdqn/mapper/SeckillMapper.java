package com.bdqn.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.base.Q;
import com.bdqn.pojo.Seckill;
import org.apache.ibatis.annotations.Param;

/**
 * 秒杀活动
 *
 * @author LILIBO
 * @since 2022-06-24
 */
public interface SeckillMapper extends IMapper<Seckill> {

    /**
     * 获取秒杀活动分页数据
     *
     * @param page
     * @param param
     * @return
     */
    IPage<Seckill> getPageList(Page page, @Param("q") Q param);

    /**
     * 商品秒杀
     *
     * @param seckillId 秒杀活动ID
     * @param version 乐观锁版本号
     * @return
     */
    long viebuy(@Param("seckillId") Long seckillId, @Param("version") Integer version);


}
