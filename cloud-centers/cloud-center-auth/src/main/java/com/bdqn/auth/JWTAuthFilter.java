package com.bdqn.auth;

import com.alibaba.fastjson.JSON;
import com.bdqn.redis.util.JedisUtil;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import redis.clients.jedis.Jedis;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * JWT安全验证过滤器
 *
 * @author LILIBO
 * @since 2023-02-23
 */
@Component
public class JWTAuthFilter extends OncePerRequestFilter {

    /** HTTP头部令牌参数Token */
    public static final String KEY_TOKEN = "token";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        // 获取Token
        String token = request.getHeader(KEY_TOKEN);
        if (!StringUtils.hasText(token)) {
            // Token为空，不在这里处理，直接放行
            filterChain.doFilter(request, response);
            return;
        }
        // 解析Token
        String uid; // 用户ID
        try {
            uid = JWTAuth.parseJWTSubject(token);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Token非法");
        }
        // 从Redis中获取用户信息
        String cacheUser = null;
        // 从资源池中获取jedis对象用于操作Redis
        Jedis jedis = JedisUtil.getJedis();
        try {
            // 从Redis中移除用户数据
            cacheUser = jedis.get(JWTAuth.KEY_REDIS_AUTH_USER + uid);
        } catch (RuntimeException e) {
            throw new RuntimeException("用户未登录");
        } finally {
            // 用完后将jedis对象返回资源池中
            JedisUtil.close(jedis);
        }
        if(Objects.isNull(cacheUser)) {
            throw new RuntimeException("用户未登录");
        }
        SecurityUser securityUser = JSON.parseObject(cacheUser, SecurityUser.class);
        if(Objects.isNull(securityUser)) {
            throw new RuntimeException("用户未登录");
        }

        // 获取权限信息封装到Authentication中
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(securityUser, null, securityUser.getAuthorities());
        // 将Authentication存入SecurityContextHolder中
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);


        // 过滤器放行
        filterChain.doFilter(request, response);
    }
}
