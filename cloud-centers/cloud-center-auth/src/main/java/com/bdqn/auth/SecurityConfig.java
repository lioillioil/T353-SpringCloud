package com.bdqn.auth;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.annotation.Resource;

/**
 * SecurityConfig
 *
 * @author LILIBO
 * @since 2023-02-23
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true) // 开启权限控制方案
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * 允许匿名访问的接口 "/**" 表示允许所有
     */
    private static final String ALLOW_ALL = "/**"; // 临时放开所有请求

    /**
     * 允许匿名访问的接口列表 [登录接口, 验证接口]
     */
    private static final String[] MATCHERS = {"/auth/login", "/auth/verify"};

    @Resource
    private JWTAuthFilter jwtAuthFilter;

    @Resource
    private AuthenticationEntryPoint authenticationEntryPoint;

    @Resource
    private AccessDeniedHandler accessDeniedHandler;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            // 关闭CSRF
            .csrf().disable()
            // 不通过Session获取SecurityContext
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .authorizeRequests()
            // 对于登录接口、验证接口 允许匿名访问
            .antMatchers(MATCHERS).anonymous()
            // 除上面外的所有请求全部需要鉴权认证
            .anyRequest().authenticated()
            .and()
            // 允许跨域
            .cors();

        // 把JWT安全校验过滤器添加到过滤器链中
        http.addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class);

        // 配置认证、授权自定义异常处理
        http.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint).accessDeniedHandler(accessDeniedHandler);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    public static void main(String[] args) {
        // 明文密码
        String password = "123456";
        // 创建加密对象
        BCryptPasswordEncoder bc = new BCryptPasswordEncoder();
        // 执行加密
        String enPwd = bc.encode(password);
        System.out.println(enPwd); // $2a$10$pn.vIl5oAEZHAXFpIeVzE.juqKILQFkbRWodM3/dOLviJYQYGOSQO

        // 使用明文密码和加密后的密码进行比对
        System.out.println(bc.matches(password, enPwd));
    }

}
