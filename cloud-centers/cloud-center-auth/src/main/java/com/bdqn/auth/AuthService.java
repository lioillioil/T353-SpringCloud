package com.bdqn.auth;

import com.bdqn.pojo.User;

/**
 * AuthService
 *
 * @author LILIBO
 * @since 2023-02-23
 */
public interface AuthService {

    /**
     * 登录（获取Token）
     *
     * @param user
     * @return
     */
    String login(User user);

    /**
     * 验证（验证Token合法性）
     *
     * @param token
     * @return
     */
    boolean verify(String token);

    /**
     * 退出（移除Redis中的对象信息）
     *
     * @param user
     * @return
     */
    boolean logout(User user);
}
