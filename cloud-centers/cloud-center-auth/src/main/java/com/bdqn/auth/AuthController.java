package com.bdqn.auth;

import com.bdqn.base.R;
import com.bdqn.logger.WebLogger;
import com.bdqn.pojo.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * AuthController
 *
 * @author LILIBO
 * @since 2023-02-22
 */
@Api(tags = "安全认证授权接口")
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Resource
    private AuthService authService;

    /**
     * 登录获取令牌
     */
    @ApiOperation("登录获取令牌接口")
    @WebLogger("登录获取令牌接口")
    @PostMapping("/login")
    public R login(@RequestBody User user) {
        // 完成登录验证，并发放Token（令牌）
        String token = authService.login(user);
        return R.success(token);
    }

    /**
     * 令牌验证
     */
    @ApiOperation("令牌验证接口")
    @WebLogger("令牌验证接口")
    @PostMapping("/verify")
    public R verify(@RequestBody String token) {
        // 解析Token进行验证，获取用户权限
        boolean flag = authService.verify(token);
        return R.success(flag);
    }

    /**
     * 退出登录
     */
    @ApiOperation("退出登录")
    @WebLogger("退出登录")
    @PostMapping("/logout")
    public R logout() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        SecurityUser loginUser = (SecurityUser) authentication.getPrincipal();
        boolean logout = authService.logout(loginUser.getUser());
        return R.success(logout);
    }

}
