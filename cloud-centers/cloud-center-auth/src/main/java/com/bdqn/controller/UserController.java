package com.bdqn.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.base.Q;
import com.bdqn.base.R;
import com.bdqn.logger.WebLogger;
import com.bdqn.pojo.User;
import com.bdqn.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户表
 *
 * @author LILIBO
 * @since 2022-06-24
 */
@Api(tags = "用户表接口")
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    /**
     * 新增用户表记录
     *
     * @param user
     * @return R
     */
    @ApiOperation("新增用户表记录")
    @WebLogger("新增用户表记录")
    @PostMapping("/add")
    public R save(@ApiParam("用户表对象") @RequestBody User user) {
        return R.success(userService.save(user));
    }

    /**
     * 修改用户表记录
     *
     * @param user
     * @return R
     */
    @ApiOperation("修改用户表记录")
    @WebLogger("修改用户表记录")
    @PostMapping("/update")
    public R update(@ApiParam("用户表对象") @RequestBody User user) {
        return R.success(userService.updateById(user));
    }

    /**
     * 通过id删除一条用户表记录
     *
     * @param id
     * @return R
     */
    @ApiOperation("通过id删除一条用户表记录")
    @WebLogger("通过id删除一条用户表记录")
    @PostMapping("/delete")
    public R deleteById(@ApiParam("用户表id") Long id) {
        return R.success(userService.removeById(id));
    }

    /**
     * 通过多个id批量删除用户表记录
     *
     * @param ids
     * @return R
     */
    @ApiOperation("通过多个id批量删除用户表记录")
    @WebLogger("通过多个id批量删除用户表记录")
    @PostMapping("/remove")
    public R removeByIds(@ApiParam("用户表id集合") @RequestBody List<Long> ids) {
        return R.success(userService.removeByIds(ids));
    }

    /**
     * 通过id查询单条用户表记录
     *
     * @param id
     * @return R
     */
    @ApiOperation("通过id查询单条用户表记录")
    @WebLogger("通过id查询单条用户表记录")
    @GetMapping("/{id}")
    public R<User> getById(@ApiParam("用户表id") @PathVariable Long id) {
        return R.success(userService.getById(id));
    }

    /**
     * 通过用户表信息查询用户表记录
     *
     * @param user 用户表信息对象
     * @return R
     */
    @ApiOperation("通过用户表信息查询用户表记录")
    @WebLogger("通过用户表信息查询用户表记录")
    @GetMapping("/list")
    public R<List<User>> getList(@ApiParam("查询的用户表对象") User user) {
        return R.success(userService.getList(user));
    }

    /**
     * 分页查询用户表
     *
     * @param pageNo 当前页
     * @param pageSize 每页条数
     * @param user 用户表
     * @return
     */
    @ApiOperation("分页查询用户表")
    @WebLogger("分页查询用户表")
    @GetMapping("/page")
    public R<IPage<User>> getPage(@ApiParam("当前页") @RequestParam(required = false, defaultValue = "1") Long pageNo, @ApiParam("每页条数") @RequestParam(required = false, defaultValue = "10") Long pageSize, @ApiParam("查询的用户表对象") User user) {
        return R.success(userService.getPage(new Page<>(pageNo, pageSize), user));
    }

    /**
     * 分页查询用户表数据列表
     *
     * @param pageNo 当前页
     * @param pageSize 每页条数
     * @param param 通用查询参数
     * @return 返回分页数据列表，其中count为数据总量
     */
    @ApiOperation("分页查询用户表数据列表")
    @WebLogger("分页查询用户表数据列表")
    @GetMapping("/pagelist")
    public R<List<User>> getPageList(@ApiParam("当前页") @RequestParam(required = false, defaultValue = "1") Long pageNo, @ApiParam("每页条数") @RequestParam(required = false, defaultValue = "10") Long pageSize, @ApiParam("通用查询参数") Q param) {
        IPage<User> page = userService.getPageList(new Page<>(pageNo, pageSize), param);
        return R.success(page.getTotal(), page.getRecords());
    }

}
