package com.bdqn.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.base.Q;
import com.bdqn.pojo.Power;
import org.apache.ibatis.annotations.Param;

/**
 * 菜单角色关联表
 *
 * @author LILIBO
 * @since 2022-06-24
 */
public interface PowerMapper extends IMapper<Power> {

    /**
     * 获取菜单角色关联表分页数据
     *
     * @param page
     * @param param
     * @return
     */
    IPage<Power> getPageList(Page page, @Param("q") Q param);

}
