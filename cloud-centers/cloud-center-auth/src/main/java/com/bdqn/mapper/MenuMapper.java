package com.bdqn.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.base.Q;
import com.bdqn.pojo.Menu;
import org.apache.ibatis.annotations.Param;

/**
 * 菜单表
 *
 * @author LILIBO
 * @since 2022-06-24
 */
public interface MenuMapper extends IMapper<Menu> {

    /**
     * 获取菜单表分页数据
     *
     * @param page
     * @param param
     * @return
     */
    IPage<Menu> getPageList(Page page, @Param("q") Q param);

}
