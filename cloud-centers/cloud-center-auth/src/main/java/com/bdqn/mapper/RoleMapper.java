package com.bdqn.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.base.Q;
import com.bdqn.pojo.Role;
import org.apache.ibatis.annotations.Param;

/**
 * 角色表
 *
 * @author LILIBO
 * @since 2022-06-24
 */
public interface RoleMapper extends IMapper<Role> {

    /**
     * 获取角色表分页数据
     *
     * @param page
     * @param param
     * @return
     */
    IPage<Role> getPageList(Page page, @Param("q") Q param);

}
