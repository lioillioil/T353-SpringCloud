package com.bdqn.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.base.Q;
import com.bdqn.pojo.Power;

/**
 * 菜单角色关联表
 *
 * @author LILIBO
 * @since 2022-06-24
 */
public interface PowerService extends IService<Power> {

    /**
     * 获取菜单角色关联表分页数据
     *
     * @param page
     * @param param
     * @return
     */
    IPage<Power> getPageList(Page page, Q param);

}
