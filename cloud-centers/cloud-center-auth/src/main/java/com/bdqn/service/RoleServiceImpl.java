package com.bdqn.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.base.Q;
import com.bdqn.mapper.RoleMapper;
import com.bdqn.pojo.Role;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 角色表
 *
 * @author LILIBO
 * @since 2022-06-24
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Resource
    private RoleMapper roleMapper;

    /**
     * 获取角色表分页数据
     *
     * @param page
     * @param param
     * @return
     */
    @Override
    public IPage<Role> getPageList(Page page, Q param) {
        return roleMapper.getPageList(page, param);
    }

}
