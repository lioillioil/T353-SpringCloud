package com.bdqn.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.base.Q;
import com.bdqn.pojo.Role;

/**
 * 角色表
 *
 * @author LILIBO
 * @since 2022-06-24
 */
public interface RoleService extends IService<Role> {

    /**
     * 获取角色表分页数据
     *
     * @param page
     * @param param
     * @return
     */
    IPage<Role> getPageList(Page page, Q param);

}
