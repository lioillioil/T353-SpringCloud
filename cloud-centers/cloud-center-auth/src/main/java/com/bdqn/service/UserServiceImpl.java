package com.bdqn.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.base.Q;
import com.bdqn.mapper.UserMapper;
import com.bdqn.pojo.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 用户表
 *
 * @author LILIBO
 * @since 2022-06-24
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Resource
    private UserMapper userMapper;

    /**
     * 获取用户表分页数据
     *
     * @param page
     * @param param
     * @return
     */
    @Override
    public IPage<User> getPageList(Page page, Q param) {
        return userMapper.getPageList(page, param);
    }

}
