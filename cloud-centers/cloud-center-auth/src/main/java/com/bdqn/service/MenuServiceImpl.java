package com.bdqn.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.base.Q;
import com.bdqn.mapper.MenuMapper;
import com.bdqn.pojo.Menu;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 菜单表
 *
 * @author LILIBO
 * @since 2022-06-24
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {

    @Resource
    private MenuMapper menuMapper;

    /**
     * 获取菜单表分页数据
     *
     * @param page
     * @param param
     * @return
     */
    @Override
    public IPage<Menu> getPageList(Page page, Q param) {
        return menuMapper.getPageList(page, param);
    }

}
