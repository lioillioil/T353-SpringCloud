package com.bdqn.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.base.Q;
import com.bdqn.mapper.PowerMapper;
import com.bdqn.pojo.Power;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 菜单角色关联表
 *
 * @author LILIBO
 * @since 2022-06-24
 */
@Service
public class PowerServiceImpl extends ServiceImpl<PowerMapper, Power> implements PowerService {

    @Resource
    private PowerMapper powerMapper;

    /**
     * 获取菜单角色关联表分页数据
     *
     * @param page
     * @param param
     * @return
     */
    @Override
    public IPage<Power> getPageList(Page page, Q param) {
        return powerMapper.getPageList(page, param);
    }

}
