package com.bdqn.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.base.Q;
import com.bdqn.pojo.User;

/**
 * 用户表
 *
 * @author LILIBO
 * @since 2022-06-24
 */
public interface UserService extends IService<User> {

    /**
     * 获取用户表分页数据
     *
     * @param page
     * @param param
     * @return
     */
    IPage<User> getPageList(Page page, Q param);

}
