<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <!-- 微服务项目的父工程组件 -->
    <groupId>com.bdqn</groupId>
    <artifactId>T353-SpringCloud</artifactId>
    <version>1.0-SNAPSHOT</version>
    <!-- 打包方式：pom -->
    <packaging>pom</packaging>

    <!-- 微服务项目名称 -->
    <name>T353-SpringCloud</name>

    <!-- 微服务项目所包含的子模块 -->
    <modules>
        <!-- 微服务核心模块 -->
        <module>cloud-centers</module>
        <module>cloud-commons</module>
        <module>cloud-gateway</module>
        <module>cloud-generator</module>
        <module>cloud-listeners</module>
        <module>cloud-proxy-apis</module>
        <module>cloud-service-order</module>
        <module>cloud-service-payment</module>
        <module>cloud-service-seckill</module>
    </modules>

    <!-- 统一管理版本 -->
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <!-- SpringBoot的版本需要根据SpringCloud的版本要求进行确定，这里使用SpringCloud 2021.0.5版本，对应SpringBoot 2.7.15版本，使用SpringCloud Alibaba 2021.1版本 -->
        <spring.boot.version>2.7.18</spring.boot.version>
        <spring.cloud.version>2021.0.5</spring.cloud.version>
        <alibaba.cloud.version>2021.0.5.0</alibaba.cloud.version>
        <lombok.version>1.18.20</lombok.version>
        <mysql.version>8.2.0</mysql.version>
        <druid.version>1.2.15</druid.version>
        <dynamic.version>3.5.2</dynamic.version>
        <mybatis-plus.version>3.5.5</mybatis-plus.version>
        <commons.version>3.12.0</commons.version>
        <hutool.version>5.8.10</hutool.version>
        <okio.version>3.3.0</okio.version>
        <okhttp.version>2.7.5</okhttp.version>
        <fastjson.version>2.0.24</fastjson.version>
        <jjwt.version>0.9.1</jjwt.version>
        <java-jwt.version>4.3.0</java-jwt.version>
        <swagger.version>3.0.0</swagger.version>
        <cloud.version>1.0-SNAPSHOT</cloud.version>
    </properties>

    <!-- 引入项目的基础依赖，所有子模块都需要的包 -->
    <dependencies>
        <!-- 支持自动Getter/Setter -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional><!-- 声明当前依赖是可选的，默认情况下不会被其他项目继承 -->
        </dependency>
        <!-- SpringBoot热部署支持（开发阶段使用） -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <optional>true</optional>
        </dependency>
        <!-- SpringBoot框架测试支持 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <!-- Slf4j日志框架 -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
        </dependency>
    </dependencies>

    <!-- 在父工程中统一定义依赖包的版本，子模块继承之后可以锁定版本，可以不用写version以便统一版本 -->
    <dependencyManagement>
        <dependencies>
            <!-- SpringBoot框架 -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring.boot.version}</version>
                <type>pom</type><!-- 所依赖项目类型，pom表示导入依赖 -->
                <scope>import</scope><!-- 将所依赖项目中dependencyManagement依赖管理所定义的依赖导入，导入后可在本项目中使用 -->
            </dependency>
            <!-- SpringCloud微服务框架 -->
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring.cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <!-- SpringCloud Alibaba微服务支持 -->
            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-alibaba-dependencies</artifactId>
                <version>${alibaba.cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <!-- MySQL数据库连接驱动 -->
            <dependency>
                <groupId>com.mysql</groupId>
                <artifactId>mysql-connector-j</artifactId>
                <version>${mysql.version}</version>
            </dependency>
            <!-- 阿里巴巴Druid数据源 -->
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>druid-spring-boot-starter</artifactId>
                <version>${druid.version}</version>
            </dependency>
            <!-- 定义MyBatis-Plus支持 -->
            <dependency>
                <groupId>com.baomidou</groupId>
                <artifactId>mybatis-plus-boot-starter</artifactId>
                <version>${mybatis-plus.version}</version>
            </dependency>
            <!-- 定义MyBatis-Plus动态数据源支持 -->
            <dependency>
                <groupId>com.baomidou</groupId>
                <artifactId>dynamic-datasource-spring-boot-starter</artifactId>
                <version>${dynamic.version}</version>
            </dependency>
            <!-- Swagger接口管理 -->
            <dependency>
                <groupId>io.springfox</groupId>
                <artifactId>springfox-boot-starter</artifactId>
                <version>${swagger.version}</version>
            </dependency>
            <!-- 公共操作工具包 -->
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-lang3</artifactId>
                <version>${commons.version}</version>
            </dependency>
            <!-- HuTool工具包 -->
            <dependency>
                <groupId>cn.hutool</groupId>
                <artifactId>hutool-all</artifactId>
                <version>${hutool.version}</version>
            </dependency>
            <dependency>
                <groupId>com.squareup.okio</groupId>
                <artifactId>okio</artifactId>
                <version>${okio.version}</version>
            </dependency>
            <dependency>
                <groupId>com.squareup.okhttp</groupId>
                <artifactId>okhttp</artifactId>
                <version>${okhttp.version}</version>
            </dependency>
            <!-- FastJSON支持 -->
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>fastjson</artifactId>
                <version>${fastjson.version}</version>
            </dependency>
            <!-- 定义JWT -->
            <dependency>
                <groupId>io.jsonwebtoken</groupId>
                <artifactId>jjwt</artifactId>
                <version>${jjwt.version}</version>
            </dependency>
            <dependency>
                <groupId>com.auth0</groupId>
                <artifactId>java-jwt</artifactId>
                <version>${java-jwt.version}</version>
            </dependency>
            <!-- 定义公共基础模块 -->
            <dependency>
                <groupId>com.bdqn</groupId>
                <artifactId>cloud-common-base</artifactId>
                <version>${cloud.version}</version>
            </dependency>
            <!-- 定义公共接口模块 -->
            <dependency>
                <groupId>com.bdqn</groupId>
                <artifactId>cloud-common-apis</artifactId>
                <version>${cloud.version}</version>
            </dependency>
            <!-- 定义公共模型层模块 -->
            <dependency>
                <groupId>com.bdqn</groupId>
                <artifactId>cloud-common-model</artifactId>
                <version>${cloud.version}</version>
            </dependency>
            <!-- 定义公共工具模块 -->
            <dependency>
                <groupId>com.bdqn</groupId>
                <artifactId>cloud-common-util</artifactId>
                <version>${cloud.version}</version>
            </dependency>
            <!-- 定义公共Redis模块 -->
            <dependency>
                <groupId>com.bdqn</groupId>
                <artifactId>cloud-common-redis</artifactId>
                <version>${cloud.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <!-- 编译配置 -->
    <build>
        <!-- 插件配置 -->
        <plugins>
            <!-- SpringBoot的Maven插件 -->
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>2.7.18</version>
                <configuration>
                    <fork>true</fork><!-- 用于明确表示编译版本配置有效；如果没有该配置，使用SpringBoot的热加载devtools不会生效 -->
                    <addResources>true</addResources><!-- 添加资源文件 -->
                </configuration>
            </plugin>
            <!-- Maven的测试运行器插件 -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.22.2</version>
                <!-- 配置插件 -->
                <configuration>
                    <skipTests>true</skipTests><!-- 打包时跳过测试 -->
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>
