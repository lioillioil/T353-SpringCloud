package com.bdqn.logger;

import java.lang.annotation.*;

/**
 * WebLogger
 *
 * @author LILIBO
 * @since 2021-06-24
 */
// JDK中的元注解用来修饰注解，主要包括：@Target（表明该注解可以应用的Java元素类型）、@Retention（表明该注解的生命周期）、@Documented（表明该注解标记的元素可以被Javadoc或类似的工具文档化）、@Inherited（表明使用了@Inherited注解的注解，所标记的类的子类也会拥有这个注解）

// @Target元注解，表明该注解可以用应用在哪里，可用ElementType枚举取值（10种），常用的有以下3种：
// ElementType.TYPE 表示应用于类、接口（包括注解类型）、枚举
// ElementType.FIELD 表示应用于属性（包括枚举中的常量）
// ElementType.METHOD 表示应用于方法
@Target({ElementType.TYPE, ElementType.METHOD})

// @Retention元注解，表明该注解的生命周期，可用RetentionPolicy枚举取值（3种），如下：
// RetentionPolicy.SOURCE 编译时被丢弃，不包含在类文件中
// RetentionPolicy.CLASS JVM加载时被丢弃，包含在类文件中，默认值
// RetentionPolicy.RUNTIME 由JVM加载，包含在类文件中，在运行时可以被获取到
@Retention(RetentionPolicy.RUNTIME)

// @Documented 表明该注解标记的元素可以被Javadoc 或类似的工具文档化
@Documented
public @interface WebLogger {
    String value() default "";
}
