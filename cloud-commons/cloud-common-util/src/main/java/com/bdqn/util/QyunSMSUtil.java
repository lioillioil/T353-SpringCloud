package com.bdqn.util;

import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.sms.v20190711.SmsClient;
import com.tencentcloudapi.sms.v20190711.models.SendSmsRequest;
import com.tencentcloudapi.sms.v20190711.models.SendSmsResponse;

import java.util.Arrays;
import java.util.Random;

/**
 * QyunSMSUtil腾讯云验证码短信工具
 *
 * @author LILIBO
 * @since 2021-06-24
 */
public class QyunSMSUtil {

    // 访问地址
    private static final String ENDPOINT = "sms.tencentcloudapi.com";
    // 服务区域
    private static final String ORGION = "ap-guangzhou";
    // API密钥ID
    private static final String SECRETID = "AKIDTlodEagX9WeXAYgYBZhZBfuue9PQBhy0";
    // API密钥Key
    private static final String SECRETKEY = "pUw96oHVTDiyyjEUVHsITO4UfbJlywe7";

    // 短信应用ID
    private static final String SMSSDKAPPID = "1400711755";
    // 短信签名（【李利波个人公众号】）
    private static final String SIGNNAME = "李利波个人公众号";
    // 短信模板ID（您的登录验证码为{1}，{2}分钟内有效。请勿泄露于他人！如非本人操作请忽略。）
    private static final String TEMPLATEID = "1486709";

    // 生成的验证码随机范围
    private static final String NUMBERCHAR = "0123456789";

    // 短信发送成功（"Code":"Ok"）
    private static final String CODE_OK = "Ok";

    /**
     * 腾讯云验证码短信发送
     *
     * @param args
     */
    public static void main(String[] args) {
        // 调用腾讯云短信接口发送验证码短信，发送成功返回生成的随机验证码
        String result = QyunSMSUtil.sendSMS("15989062608");
        System.out.println(result!=null ? result + " - 验证码短信发送成功！" : "验证码短信发送失败！");
    }

    /**
     * 腾讯云短信验证码发送
     *
     * @param phone 接收者手机号
     * @return 返回随机生成的验证码
     */
    public static String sendSMS(String phone) {
        StringBuffer strbuf = new StringBuffer();
        Random random = new Random();
        for (int i = 0; i < 6; i++) { // 产生6位随机数字
            strbuf.append(NUMBERCHAR.charAt(random.nextInt(NUMBERCHAR.length())));
        }
        String code = strbuf.toString();

        String[] phoneNumberSet = {phone};
        String[] templateParamSet = {code, "2"};
        if (sendSMS(phoneNumberSet, templateParamSet)) {
            // 如果验证码短信发送成功则返回该验证码
            return code;
        }
        return null;
    }

    /**
     * 腾讯云短信验证码发送
     *
     * @param phone 接收者手机号
     * @param code 短信验证码
     * @return 是否成功
     */
    public static boolean sendSMS(String phone, String code) {
        String[] phoneNumberSet = {phone};
        String[] templateParamSet = {code, "2"};
        return sendSMS(phoneNumberSet, templateParamSet);
    }

    /**
     * 腾讯云短信验证码发送
     *
     * @param phoneNumberSet 接收者手机号（可多个）
     * @param templateParamSet 模板参数（根据模板按顺序设置）
     * @return 是否成功
     */
    public static boolean sendSMS(String[] phoneNumberSet, String[] templateParamSet) {
        try {
            // 实例化一个认证对象，入参需要传入腾讯云账户secretId，secretKey,此处还需注意密钥对的保密
            // 密钥可前往https://console.cloud.tencent.com/cam/capi网站进行获取
            Credential cred = new Credential(SECRETID, SECRETKEY);
            // 实例化一个http选项，可选的，没有特殊需求可以跳过
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint(ENDPOINT);
            // 实例化一个client选项，可选的，没有特殊需求可以跳过
            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);
            // 实例化要请求产品的client对象,clientProfile是可选的
            SmsClient client = new SmsClient(cred, ORGION, clientProfile);
            // 实例化一个请求对象,每个接口都会对应一个request对象
            SendSmsRequest req = new SendSmsRequest();
            // String[] phoneNumberSet = {"15989062608"};
            req.setPhoneNumberSet(phoneNumberSet);

            // 短信应用签名及模板
            req.setSmsSdkAppid(SMSSDKAPPID);
            req.setSign(SIGNNAME);
            req.setTemplateID(TEMPLATEID);

            // 模板参数（{"验证码", "有效时间"}）
            // String[] templateParamSet = {"9527", "2"};
            req.setTemplateParamSet(templateParamSet);

            // 返回的resp是一个SendSmsResponse的实例，与请求对象对应
            SendSmsResponse resp = client.SendSms(req);
            // 输出json格式的字符串回包
            System.out.println(SendSmsResponse.toJsonString(resp));
            return CODE_OK.equals(Arrays.stream(resp.getSendStatusSet()).findFirst().get().getCode());
        } catch (TencentCloudSDKException e) {
            e.printStackTrace();
        }
        return false;
    }

}
