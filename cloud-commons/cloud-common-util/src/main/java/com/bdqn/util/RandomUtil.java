package com.bdqn.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * 随机生成工具类
 *
 * @author LILIBO
 * @since 2021-06-24
 */
public class RandomUtil {

    /**
     * 所有, 包含所有数字和大小写字母
     */
    public static final int CHAR_ALL = 1;

    /**
     * 完整, 包含所有数字和字母
     */
    public static final int CHAR_INTACT = 2;

    /**
     * 字母, 包含所有字母
     */
    public static final int CHAR_LETTER = 3;

    /**
     * 数字, 包含所有数字
     */
    public static final int CHAR_NUMBER = 4;

    /**
     * HEX, 十六进制数字和字母
     */
    public static final int CHAR_HEX = 5;

    private static final String allChar = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private static final String intactChar = "0123456789abcdefghijklmnopqrstuvwxyz";

    private static final String letterChar = "abcdefghijklmnopqrstuvwxyz";

    private static final String numberChar = "0123456789";

    private static final String hexChar = "0123456789ABCDEF";

    /**
     * 生成UUID字符串，去除"-"
     *
     * @return UUID
     */
    public static String generateUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
    }

    /**
     * 随机生成字符串, 大小写混合
     *
     * @param length 生成的长度
     * @return 随机字符串
     */
    public static String generateRandom(int length) {
        StringBuffer strbuf = new StringBuffer();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            strbuf.append(allChar.charAt(random.nextInt(allChar.length())));
        }
        return strbuf.toString();
    }

    /**
     * 随机生成字符串, 根据类型输出
     *
     * @param length 生成的长度
     * @param type 生成的类型
     * @return 随机字符串
     */
    public static String generateRandom(int length, int type) {
        String chars = allChar;
        switch (type) {
            case CHAR_ALL:
                chars = allChar;
                break;
            case CHAR_INTACT:
                chars = intactChar;
                break;
            case CHAR_LETTER:
                chars = letterChar;
                break;
            case CHAR_NUMBER:
                chars = numberChar;
                break;
            case CHAR_HEX:
                chars = hexChar;
                break;
            default:
                chars = allChar;
                break;
        }
        StringBuffer strbuf = new StringBuffer();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            strbuf.append(chars.charAt(random.nextInt(chars.length())));
        }
        return strbuf.toString();
    }

    private static Random random = new Random();

    public static int getRandom(int min, int max) {
        if (min == max) {
            return min;
        }
        if (min > max) {
            return min;
        }
        return Math.abs(random.nextInt()) % (max - min + 1) + min;
    }

    public static int random(int Min, int Max) {
        return (int) Math.round(Math.random() * (Max - Min) + Min);
    }

    /**
     * 随机抽取n个不同的数
     */
    public static List<String> selectNum(List<String> arrayList, int num) {
        List<String> list = new ArrayList<String>();
        int r = 0;
        for (int i = 1; i <= num; i++) {
            r = getRandom(0, arrayList.size() - i);
            list.add(arrayList.get(r));
            // 随机到得元素与未互换过位置的最后一个元素位置互换
            arrayList.set(r, arrayList.get(arrayList.size() - i));
        }
        return list;
    }

    public static void main(String[] args) {
        // String restus = UUID.randomUUID().toString().toUpperCase();
        String restus = System.currentTimeMillis() + "";
        // String restus = generateUUID().toUpperCase();
        // String restus = generateRandom(4, RandomUtil.CHAR_INTACT);
        // System.out.println(randomDouble(2.12,12.312));
        int i = 982;
        double start = Double.valueOf("9.21");
        int start2 = Integer.valueOf(String.valueOf(Math.round(Double.valueOf("9.21") * 100)));
        long start3 = Math.round(Double.valueOf("9.21") * 100);
        // double s = Double.valueOf(i)/100;
        System.err.println(start2);
    }

}
