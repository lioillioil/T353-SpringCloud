package com.bdqn.util;

import java.math.BigDecimal;

/**
 * 数字计算工具类
 *
 * @author LILIBO
 * @since 2021-06-24
 */
public class NumberUtil {

    /**
     * 保留小数点位数（向上舍入）
     *
     * @param val 操作值
     * @param digit 小数点位数
     * @return
     */
    public static double decimal(double val, int digit) {
        return decimal(val, digit, BigDecimal.ROUND_HALF_DOWN);
    }

    /**
     * 保留小数点位数
     *
     * @param val 操作值
     * @param digit 小数点位数
     * @param roundingMode 舍入模式
     * @return
     * @see
     */
    public static double decimal(double val, int digit, int roundingMode) {
        BigDecimal bd = new BigDecimal(val);
        val = bd.setScale(digit, roundingMode).doubleValue();
        return val;
    }

}
