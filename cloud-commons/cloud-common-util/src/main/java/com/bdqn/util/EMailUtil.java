package com.bdqn.util;

import cn.hutool.extra.mail.MailAccount;
import cn.hutool.extra.mail.MailUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * EMailUtil
 *
 * @author LILIBO
 * @since 2023-01-11
 */
@Component
public class EMailUtil {

    /** 服务器地址 */
    private static String host = "smtp.163.com";

    @Resource
    @Value("${system.mail.host}")
    public void setHost(String host) {
        EMailUtil.host = host;
    }

    /** 服务器端口（默认：25） */
    private static int port = 465;

    @Resource
    @Value("${system.mail.port}")
    public void setPort(int port) {
        EMailUtil.port = port;
    }

    /** 认证（默认：true） */
    private static boolean auth = true;

    @Resource
    @Value("${system.mail.auth}")
    public void setAuth(boolean auth) {
        EMailUtil.auth = auth;
    }

    /** 发件人邮箱地址 */
    private static String from = "ServiceAlarm@163.com";

    @Resource
    @Value("${system.mail.from}")
    public void setFrom(String from) {
        EMailUtil.from = from;
    }

    /** 发件人的邮箱用户名 */
    private static String user = "ServiceAlarm";

    @Resource
    @Value("${system.mail.user}")
    public void setUser(String user) {
        EMailUtil.user = user;
    }

    /** 发件人的邮箱授权码 */
    private static String pass = "STJYLQBYAVOZYNSB";

    @Resource
    @Value("${system.mail.pass}")
    public void setPass(String pass) {
        EMailUtil.pass = pass;
    }

    /** 发件人邮箱地址 */
    private static List<String> to;

    @Resource
    @Value("#{'${system.mail.to:}'.empty ? null : '${system.mail.to:}'.split(',')}")
    public void setTo(List<String> to) {
        EMailUtil.to = to;
    }

    /**
     * 开启SSL
     */
    private static boolean sslEnable = true;
    @Resource
    @Value("${system.mail.sslEnable}")
    public void setSslEnable(boolean sslEnable) {
        EMailUtil.sslEnable = sslEnable;
    }
    private static boolean starttlsEnable = true;
    @Resource
    @Value("${system.mail.starttlsEnable}")
    public void setStarttlsEnable(boolean starttlsEnable) {
        EMailUtil.starttlsEnable = starttlsEnable;
    }
    private static boolean socketFactoryFallback = false;
    @Resource
    @Value("${system.mail.socketFactoryFallback}")
    public void setSocketFactoryFallback(boolean socketFactoryFallback) {
        EMailUtil.socketFactoryFallback = socketFactoryFallback;
    }

    /**
     * 发送简单邮件（发送到配置的接收邮箱）
     *
     * @param subject 主题
     * @param content 正文
     */
    public static void send(String subject, String content) {
        if (StringUtils.isEmpty(subject) || StringUtils.isEmpty(content)) {
            return;
        }
        MailAccount account = new MailAccount();
        account.setHost(host); // 服务器地址
        account.setPort(port); // 服务器端口（默认：25）
        account.setAuth(auth); // 认证（默认：true）
        account.setFrom(from); // 发件人邮箱地址
        account.setUser(user); // 发件人邮箱用户名
        account.setPass(pass); // 发件人的邮箱授权码
        account.setSslEnable(sslEnable);
        account.setStarttlsEnable(starttlsEnable);
        account.setSocketFactoryFallback(socketFactoryFallback);
        MailUtil.send(account, to, subject, content, true);
    }

    /**
     * 发送简单邮件（发送到指定的收件人邮箱）
     *
     * @param to 收件人列表，多个邮箱用逗号“,”分隔
     * @param subject 主题
     * @param content 正文
     */
    public static void send(String to, String subject, String content) {
        if (StringUtils.isEmpty(to) || StringUtils.isEmpty(subject) || StringUtils.isEmpty(content)) {
            return;
        }
        List<String> tos = new ArrayList<>();
        String[] toSplit = to.split(",");
        for (String s : toSplit) {
            if (StringUtils.isNotEmpty(s)) {
                tos.add(s);
            }
        }
        MailAccount account = new MailAccount();
        account.setHost(host); // 服务器地址
        account.setPort(port); // 服务器端口（默认：25）
        account.setAuth(auth); // 认证（默认：true）
        account.setFrom(from); // 发件人邮箱地址
        account.setUser(user); // 发件人邮箱用户名
        account.setPass(pass); // 发件人的邮箱授权码
        account.setSslEnable(sslEnable);
        account.setStarttlsEnable(starttlsEnable);
        account.setSocketFactoryFallback(socketFactoryFallback);
        MailUtil.send(account, tos, subject, content, true);
    }

    public static void main(String[] args) {
        EMailUtil.send("78360159@qq.com,llb@ktjiaoyu.com", "您好", "人生只有一件事，既是当下这件事。All in now！！！");
        System.out.println("邮件发送成功！");
    }
}
