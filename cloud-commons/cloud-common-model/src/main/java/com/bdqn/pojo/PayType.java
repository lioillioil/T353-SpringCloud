package com.bdqn.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 支付方式
 *
 * @author LILIBO
 * @since 2022-06-24
 */
@Api("支付方式")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName("`pay_type`")
public class PayType extends Model {

    /**
     * 支付方式ID
     */
    @ApiModelProperty("支付方式ID")
    @TableId(type = IdType.AUTO) // 自动增长ID
    private Integer id;

    /**
     * 支付方式名称
     */
    @ApiModelProperty("支付方式名称")
    private String name;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;

    /**
     * 支付方式
     */
    public PayType(String name, String remark) {
        this.name = name;
        this.remark = remark;
    }

}
