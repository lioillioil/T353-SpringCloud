package com.bdqn.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Payment 支付记录实体
 *
 * @author LILIBO
 * @since 2024-03-19
 */
@Api("支付记录")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName("`payment`")
public class Payment extends Model {

    /**
     * 支付记录ID
     */
    @ApiModelProperty("支付记录ID")
    @TableId(type = IdType.ASSIGN_ID) // 使用雪花算法生成ID
    private Long id;

    /**
     * 订单ID
     */
    @ApiModelProperty("订单ID")
    private String orderId;

    /**
     * 支付方式
     */
    @ApiModelProperty("支付方式")
    private Integer payType;

    /**
     * 支付时间
     */
    @ApiModelProperty("支付时间")
    private String payTime;

    /**
     * 支付金额
     */
    @ApiModelProperty("支付金额")
    private Double payCost;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;

    /**
     * 支付状态 0:未支付;1:已支付
     */
    @ApiModelProperty("支付状态 0:未支付;1:已支付")
    private Long payStatus;

    /**
     * 状态 0:正在处理;1:已完成
     */
    @ApiModelProperty("状态 0:正在处理;1:已完成")
    private Long status;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private String createTime;

    /**
     * 修改时间
     */
    @ApiModelProperty("修改时间")
    private String modifyTime;

    /**
     * 支付记录
     */
    public Payment(String orderId, Integer payType, String payTime, Double payCost, String remark, Long payStatus, Long status, String createTime, String modifyTime) {
        this.orderId = orderId;
        this.payType = payType;
        this.payTime = payTime;
        this.payCost = payCost;
        this.remark = remark;
        this.payStatus = payStatus;
        this.status = status;
        this.createTime = createTime;
        this.modifyTime = modifyTime;
    }

}
