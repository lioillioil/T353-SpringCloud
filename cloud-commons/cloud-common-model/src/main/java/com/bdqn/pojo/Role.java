package com.bdqn.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 角色表
 *
 * @author LILIBO
 * @since 2022-06-24
 */
@Api("角色表")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName("`role`")
public class Role extends Model {

    /**
     * 角色编号
     */
    @ApiModelProperty("角色编号")
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String name;

    /**
     * 描述
     */
    @ApiModelProperty("描述")
    private String remark;

    /**
     * 状态 0:启用;1:禁用
     */
    @ApiModelProperty("状态 0:启用;1:禁用")
    private Integer status;

}
