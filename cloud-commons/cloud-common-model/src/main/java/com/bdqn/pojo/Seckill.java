package com.bdqn.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 秒杀商品
 *
 * @author LILIBO
 * @since 2023-04-25
 */
@Api("秒杀商品")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName("`seckill`")
public class Seckill extends Model {

    /**
     * 秒杀活动ID
     */
    @ApiModelProperty("秒杀活动ID")
    @TableId(type = IdType.ASSIGN_ID) // 使用雪花算法生成ID
    private Long id;

    /**
     * 商品ID
     */
    @ApiModelProperty("商品ID")
    private Long productId;

    /**
     * 秒杀价格
     */
    @ApiModelProperty("秒杀价格")
    private Double seckillPrice;

    /**
     * 库存数量
     */
    @ApiModelProperty("库存数量")
    private Integer stockCount;

    /**
     * 开始时间
     */
    @ApiModelProperty("开始时间")
    private String startTime;

    /**
     * 结束时间
     */
    @ApiModelProperty("结束时间")
    private String endTime;

    /**
     * 乐观锁版本号
     */
    @ApiModelProperty("乐观锁版本号")
    private Integer version;

    public Seckill(Long productId, Double seckillPrice, Integer stockCount, String startTime, String endTime) {
        this.productId = productId;
        this.seckillPrice = seckillPrice;
        this.stockCount = stockCount;
        this.startTime = startTime;
        this.endTime = endTime;
    }

}
