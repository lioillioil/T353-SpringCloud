package com.bdqn.pojo;

import java.io.Serializable;

/**
 * Model 数据库模型父类
 *
 * @author LILIBO
 * @since 2024-03-23
 */
public abstract class Model implements Serializable {

    private static final long serialVersionUID = 1L;

}
