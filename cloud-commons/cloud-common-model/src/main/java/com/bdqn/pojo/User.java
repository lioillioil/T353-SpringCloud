package com.bdqn.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 用户表
 *
 * @author LILIBO
 * @since 2022-06-24
 */
@Api("用户表")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName("`sys_user`")
public class User extends Model {

    /**
     * 用户编号
     */
    @ApiModelProperty("用户编号")
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String name;

    /**
     * 密码
     */
    @ApiModelProperty("密码")
    private String password;

    /**
     * 角色ID 关联角色表ID
     */
    @ApiModelProperty("角色ID 关联角色表ID")
    private Long roleId;

    /**
     * 描述
     */
    @ApiModelProperty("描述")
    private String remark;

    /**
     * 状态 0:启用;1:禁用
     */
    @ApiModelProperty("状态 0:启用;1:禁用")
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private String createTime;

    /**
     * 修改时间
     */
    @ApiModelProperty("修改时间")
    private String modifyTime;

}
