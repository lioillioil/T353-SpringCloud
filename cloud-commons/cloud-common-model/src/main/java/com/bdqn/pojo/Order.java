package com.bdqn.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 订单
 *
 * @author LILIBO
 * @since 2023-04-25
 */
@Api("订单")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName("`order`")
public class Order extends Model {

    /**
     * 销售订单ID
     */
    @ApiModelProperty("销售订单ID")
    @TableId(type = IdType.ASSIGN_ID) // 使用雪花算法生成ID
    private Long id;

    /**
     * 订单号 例如: YJ202304161813
     */
    @ApiModelProperty("订单号 例如: YJ202304161813")
    private String orderNo;

    /**
     * 下单时间
     */
    @ApiModelProperty("下单时间")
    private String orderTime;

    /**
     * 订单金额
     */
    @ApiModelProperty("订单金额")
    private Double orderCost;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;

    /**
     * 支付状态 0:未支付;1:已支付
     */
    @ApiModelProperty("支付状态 0:未支付;1:已支付")
    private Integer payStatus;

    /**
     * 状态 0:正在处理;1:已完成
     */
    @ApiModelProperty("状态 0:正在处理;1:已完成")
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private String createTime;

    /**
     * 修改时间
     */
    @ApiModelProperty("修改时间")
    private String modifyTime;

    /**
     * 订单
     */
    public Order(String orderNo, String orderTime, Double orderCost, String remark, Integer payStatus, Integer status, String createTime, String modifyTime) {
        this.orderNo = orderNo;
        this.orderTime = orderTime;
        this.orderCost = orderCost;
        this.remark = remark;
        this.payStatus = payStatus;
        this.status = status;
        this.createTime = createTime;
        this.modifyTime = modifyTime;
    }

}
