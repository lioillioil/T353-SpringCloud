package com.bdqn.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 菜单表
 *
 * @author LILIBO
 * @since 2022-06-24
 */
@Api("菜单表")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName("`menu`")
public class Menu extends Model<Menu> {

    /**
     * 菜单编码
     */
    @ApiModelProperty("菜单编码")
    @TableId(type = IdType.AUTO)
    private String code;

    /**
     * 父级编码
     */
    @ApiModelProperty("父级编码")
    private String parentCode;

    /**
     * 类型 0:目录;1:菜单
     */
    @ApiModelProperty("类型 0:目录;1:菜单")
    private Integer type;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String text;

    /**
     * URL
     */
    @ApiModelProperty("URL")
    private String url;

    /**
     * 描述
     */
    @ApiModelProperty("描述")
    private String remark;

    /**
     * 状态 0:启用;1:禁用
     */
    @ApiModelProperty("状态 0:启用;1:禁用")
    private Integer status;

}
