package com.bdqn.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 菜单角色关联表
 *
 * @author LILIBO
 * @since 2022-06-24
 */
@Api("菜单角色关联表")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName("`power`")
public class Power extends Model {

    /**
     * 权限编号
     */
    @ApiModelProperty("权限编号")
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 角色ID
     */
    @ApiModelProperty("角色ID")
    private Long roleId;

    /**
     * 菜单ID
     */
    @ApiModelProperty("菜单ID")
    private Long menuId;

    /**
     * 状态 0:启用;1:禁用
     */
    @ApiModelProperty("状态 0:启用;1:禁用")
    private Integer status;

}
