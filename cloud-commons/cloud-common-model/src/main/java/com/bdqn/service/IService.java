package com.bdqn.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;

/**
 * Service层的父类接口，实现了基本的CRUD的封装
 *
 * @param <T> 数据对象实体
 * @author LILIBO
 * @since 2021-06-24
 */
public interface IService<T> extends com.baomidou.mybatisplus.extension.service.IService<T> {

    /**
     * 根据对象特征查询匹配的对象列表
     *
     * @param param 数据特征
     * @return 符合对象查询条件的对象数据
     */
    List<T> getList(T param);

    /**
     * 根据对象特征分页查询对象数据
     *
     * @param param 数据特征
     * @return 包含一页数据的分页对象
     */
    IPage<T> getPage(Page<T> page, T param);

}
