package com.bdqn.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.mapper.IMapper;

import java.util.List;

/**
 * Service层的父类，实现了基本的CRUD的封装
 *
 * @author LILIBO
 * @since 2021-06-24
 */
public abstract class ServiceImpl<M extends IMapper<T>, T> extends com.baomidou.mybatisplus.extension.service.impl.ServiceImpl<M, T> implements IService<T> {

    /**
     * 根据对象特征查询匹配的对象列表
     *
     * @param param 数据特征
     * @return 符合对象查询条件的对象数据
     */
    @Override
    public List<T> getList(T param) {
        return baseMapper.getList(null, param);
    }

    /**
     * 根据对象特征分页查询对象数据
     *
     * @param page 分页对象
     * @param param 数据特征
     * @return 包含一页数据的分页对象
     */
    @Override
    public IPage<T> getPage(Page<T> page, T param) {
        return page.setRecords(baseMapper.getList(page, param));
    }

}
