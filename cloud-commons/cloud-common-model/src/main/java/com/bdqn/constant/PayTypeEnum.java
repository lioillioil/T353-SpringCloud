package com.bdqn.constant;

import lombok.Getter;

/**
 * PayTypeEnum 支付类型枚举
 *
 * @author LILIBO
 * @since 2024-03-20
 */
@Getter
public enum PayTypeEnum {

    WeChatPay(0, "微信支付"),
    AliPay(1, "支付宝支付"),
    CashPay(2, "现金支付");

    private final int val;
    private final String msg;

    PayTypeEnum(int val, String msg) {
        this.val = val;
        this.msg = msg;
    }

    public static PayTypeEnum get(int val) {
        for (PayTypeEnum e : PayTypeEnum.values()) {
            if (e.getVal() == val) {
                return e;
            }
        }
        return null;
    }

    public static PayTypeEnum get(String msg) {
        for (PayTypeEnum e : PayTypeEnum.values()) {
            if (e.getMsg().equals(msg)) {
                return e;
            }
        }
        return null;
    }

}
