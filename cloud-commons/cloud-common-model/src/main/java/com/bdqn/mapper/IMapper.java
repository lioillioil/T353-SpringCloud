package com.bdqn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Mapper层的父类接口，实现了基本的CRUD的封装
 *
 * @param <T> 数据对象实体
 * @author LILIBO
 * @since 2021-06-24
 */
public interface IMapper<T> extends BaseMapper<T> {

    /**
     * 根据对象特征查询匹配的对象列表（分页查询可用同一段映射文件）
     *
     * @param page 分页对象
     * @param param 数据特征
     * @return 符合对象查询条件的对象数据
     */
    List<T> getList(Page<T> page, @Param("t") T param);

}
