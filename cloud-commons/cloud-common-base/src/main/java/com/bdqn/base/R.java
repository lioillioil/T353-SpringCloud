package com.bdqn.base;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * R 通用数据返回对象
 *
 * @author LILIBO
 * @since 2021-06-24
 */
@Api("R 数据返回对象")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class R<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 成功状态码
     */
    public static final int CODE_SUCCESS = 0;
    /**
     * 失败状态码
     */
    public static final int CODE_FAILURE = 1;

    /**
     * 成功消息
     */
    public static final String MSG_SUCCESS = "success";
    /**
     * 失败消息
     */
    public static final String MSG_FAILURE = "failure";

    /**
     * 返回状态码
     */
    @ApiModelProperty("返回状态码")
    private int code;

    /**
     * 返回消息
     */
    @ApiModelProperty("返回消息")
    private String msg;

    /**
     * 返回数据量
     */
    @ApiModelProperty("返回数据量")
    private long count;

    /**
     * 返回数据
     */
    @ApiModelProperty("返回数据")
    private T data;

    /**
     * 返回成功
     *
     * @return
     */
    public static R success() {
        return new R(CODE_SUCCESS, MSG_SUCCESS, 0L, null);
    }

    /**
     * 返回成功
     *
     * @param data
     * @return
     */
    public static R success(Object data) {
        return failure(CODE_SUCCESS, MSG_SUCCESS, data);
    }

    /**
     * 返回成功
     *
     * @param count
     * @param data
     * @return
     */
    public static R success(Long count, Object data) {
        return new R(CODE_SUCCESS, MSG_SUCCESS, count, data);
    }

    /**
     * 返回成功
     *
     * @param msg
     * @param data
     * @return
     */
    public static R success(String msg, Object data) {
        return failure(CODE_SUCCESS, msg, data);
    }

    /**
     * 返回失败
     *
     * @return
     */
    public static R failure() {
        return new R(CODE_FAILURE, MSG_FAILURE, 0L, null);
    }

    /**
     * 返回失败
     *
     * @param msg 返回消息
     * @return
     */
    public static R failure(String msg) {
        return new R(CODE_FAILURE, msg, 0L, null);
    }

    /**
     * 返回失败
     *
     * @param code 返回状态码
     * @param msg 返回消息
     * @return
     */
    public static R failure(int code, String msg) {
        return new R(code, msg, 0L, null);
    }

    /**
     * 返回失败
     *
     * @param data
     * @return
     */
    public static R failure(Object data) {
        return failure(MSG_FAILURE, data);
    }

    /**
     * 返回失败
     *
     * @param msg 返回消息
     * @param data 返回数据
     * @return
     */
    public static R failure(String msg, Object data) {
        return failure(CODE_FAILURE, msg, data);
    }

    /**
     * 返回失败
     *
     * @param code 返回状态码
     * @param msg 返回消息
     * @param data 返回数据
     * @return
     */
    public static R failure(int code, String msg, Object data) {
        long count = data instanceof List<?> ? ((List<?>) data).size() : 0L;
        return new R(code, msg, count, data);
    }

}
