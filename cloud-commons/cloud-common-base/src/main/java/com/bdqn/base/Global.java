package com.bdqn.base;

/**
 * 系统全局常量
 *
 * @author LILIBO
 * @since 2021-06-24
 */
public class Global {

    /**
     * 前端工程名
     */
    public static final String FRONT_PROJECT = "SpringCloud-UI";

    /**
     * 后端工程名
     */
    public static final String BACK_PROJECT = "SpringCloud";

    /**
     * 队列容量
     */
    public static final int CAPACITY = 10000;

    /**
     * 字符编码
     */
    public static final String CHARSET = "UTF-8";

    /**
     * 服务器默认地址
     */
    public final static String DEF_SERVER_HOST = "127.0.0.1";

    /**
     * 服务器默认端口
     */
    public static final int DEF_SERVER_PORT = 8000;

    /**
     * 缓存服务器默认地址
     */
    public final static String DEF_REDIS_HOST = "127.0.0.1";

    /**
     * 缓存服务器默认端口
     */
    public static final int DEF_REDIS_PORT = 6379;

    /**
     * 用户目录标记
     */
    public static final String USER_DIR = "user.dir";

    /**
     * HTTP头部数据类型：Content-Type
     */
    public static final String HTTP_CONTENT_TYPE = "application/json;charset=UTF-8";

    /**
     * 缓存键分割符
     */
    public static final String CACHE_POINT = ":";

    /**
     * 文件扩展名分割符
     */
    public static final String FILE_POINT = ".";

    /**
     * 文件路径分隔符
     */
    public static final String FILE_SEPAR = "/";

    /**
     * 下划线
     */
    public static final String CHAR_UN = "-";

    /**
     * 命令参数分割符
     */
    public static final String CMDS_CONF = " ";

    /**
     * 文件资源位置
     */
    public static final String SOURCE = "source";

    /**
     * 用户SessionID
     */
    public static final String SESSION = "SESSION";

    /**
     * 状态：0
     */
    public static final int STATUS_0 = 0;

    /**
     * 状态：1
     */
    public static final int STATUS_1 = 1;

    /**
     * 处理状态：进行中
     */
    public static final int STATUS_PROCESS = -1;

    /**
     * 处理状态：成功
     */
    public static final int STATUS_SUCCESS = 0;

    /**
     * 处理状态：失败
     */
    public static final int STATUS_FAILURE = 1;

    /**
     * 锁定状态：锁定
     */
    public static final int STATUS_LOCK = 9;

    /**
     * 状态信息：成功
     */
    public static final String MESSAGE_SUCCESS = "SUCCESS";

    /**
     * 状态信息：失败
     */
    public static final String MESSAGE_FAILURE = "FAILURE";

    /**
     * HTTP头部签名参数：sign
     */
    public static final String HEAD_SIGN = "sign";

    /**
     * 响应代码参数：code
     */
    public static final String PARAM_CODE = "code";

    /**
     * 消息内容参数：message
     */
    public static final String PARAM_MESSAGE = "message";

    /**
     * 唯一标识参数：uuid
     */
    public static final String PARAM_UUID = "uuid";

    /**
     * Token前缀
     */
    public static String BEARER = "BoBo";
    /**
     * 用户主体
     */
    public static String JWT_SUBJECT = "subject";
    /**
     * 用户账号
     */
    public static String JWT_ACCOUNT = "account";
    /**
     * 用户密码
     */
    public static String JWT_PASSWORD = "password";
    /**
     * Redis缓存Key
     */
    public static String KEY_USER = "user";
    /**
     * Redis缓存Key
     */
    public static String KEY_TOKEN = "token";

    /**
     * JWT加密密钥
     */
    public static String SECRET_KEY = "asd441qw1asdzx1asdas1d3aweqsad";

    /**
     * 登录方法
     */
    public static String IGNORE_LOGIN = "login";

    /**
     * 邮件消息交换机
     */
    public static final String EXCHANGE_NAME_EMAIL = "direct.cloud.email";

    /**
     * 邮件消息队列
     */
    public static final String QUEUE_NAME_EMAIL = "queue.cloud.email";

    /**
     * 邮件消息路由Key
     */
    public static final String ROUTING_KEY_EMAIL = "cloud.email";

    /**
     * 流水号
     */
    public static int SEQ = 0;

    /**
     * 获取流水号
     *
     * @return 下一个流水号
     */
    public static synchronized int nextSEQ() {
        if (SEQ == Integer.MAX_VALUE) {
            SEQ = 0;
        }
        return ++SEQ;
    }

}

