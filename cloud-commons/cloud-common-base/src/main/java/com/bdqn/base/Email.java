package com.bdqn.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * Email
 *
 * @author LILIBO
 * @since 2023-03-23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Email implements Serializable {

    private static final long serialVersionUID = 1L;

    private String to;

    private String content;

}
