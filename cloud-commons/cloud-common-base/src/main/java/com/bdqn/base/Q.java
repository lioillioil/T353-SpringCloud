package com.bdqn.base;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Q 通用查询封装对象
 *
 * @author LILIBO
 * @since 2022-06-24
 */
@Api("Q 通用查询封装对象")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Q implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 查询编号
     */
    @ApiModelProperty("查询编号")
    private Integer id;

    /**
     * 查询名称（完全匹配）
     */
    @ApiModelProperty("查询名称（完全匹配）")
    private String name;

    /**
     * 查询关键字（模糊匹配）
     */
    @ApiModelProperty("查询关键字（模糊匹配）")
    private String keyword;

    /**
     * 查询类型
     */
    @ApiModelProperty("查询类型")
    private Integer type;

    /**
     * 查询状态
     */
    @ApiModelProperty("查询状态")
    private Integer status;

    /**
     * 查询开始时间
     */
    @ApiModelProperty("查询开始时间")
    private String beginDate;

    /**
     * 查询结束时间
     */
    @ApiModelProperty("查询结束时间")
    private String endDate;

    /**
     * JSON格式参数
     */
    @ApiModelProperty("JSON格式参数")
    private String jsonAttr; // {pid:2,cid:5,qid:8}

    /**
     * 通过jsonAttr参数映射到Map集合（可在Mapper映射文件中使用参数）
     */
    @ApiModelProperty("通过jsonAttr参数映射到Map集合（可在Mapper映射文件中使用参数）")
    private Map<String, Object> attr;

    /**
     * 通过jsonAttr参数解析JSON格式并映射到Map集合
     *
     * @param jsonAttr
     */
    public void setJsonAttr(String jsonAttr) {
        this.jsonAttr = jsonAttr.trim();
        if (StringUtils.isNotEmpty(this.jsonAttr) && this.jsonAttr.startsWith("{") && this.jsonAttr.endsWith("}")) {
            JSONObject parse = JSONObject.parseObject(this.jsonAttr);
            this.attr = new HashMap<>();
            this.attr.putAll(parse);
        }
    }

}
