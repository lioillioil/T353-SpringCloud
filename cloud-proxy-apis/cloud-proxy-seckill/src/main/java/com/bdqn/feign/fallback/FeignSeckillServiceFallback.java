package com.bdqn.feign.fallback;

import com.bdqn.base.R;
import com.bdqn.feign.FeignSeckillService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * FeignOrderServiceFallback
 *
 * @author LILIBO
 * @since 2024-03-20
 */
@Slf4j
@Component
public class FeignSeckillServiceFallback implements FeignSeckillService {

    /**
     * 商品秒杀
     *
     * @param seckillId 秒杀活动ID
     */
    @Override
    public R viebuy(Long seckillId) {
        log.debug("[服务降级] {}", seckillId);
        return R.failure("系统繁忙，请稍后再试~");
    }

}
