package com.bdqn.feign;

import com.bdqn.base.R;
import com.bdqn.feign.fallback.FeignSeckillServiceFallback;
import com.bdqn.logger.WebLogger;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * FeignSeckillService 秒杀服务远程调用接口
 *
 * @author LILIBO
 * @since 2024-03-20
 */
@FeignClient(name = "cloud-seckill-serve", fallback = FeignSeckillServiceFallback.class)
public interface FeignSeckillService {

    /**
     * 商品秒杀
     *
     * @param seckillId 秒杀活动ID
     */
    @WebLogger("商品秒杀")
    @PostMapping("/seckill/viebuy")
    R viebuy(@RequestParam("seckillId") Long seckillId);

}
