package com.bdqn.feign.fallback;

import com.bdqn.base.R;
import com.bdqn.feign.FeignAuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * FeignAuthServiceFallback
 *
 * @author LILIBO
 * @since 2023-03-11
 */
@Slf4j
@Component
public class FeignAuthServiceFallback implements FeignAuthService {

    /**
     * 认证模块 远程调用进行Token验证
     *
     * @return 认证是否成功
     */
    @Override
    public R verify(@RequestBody String token) {
        log.debug("[服务降级] verify --> token: {0}", token);
        return R.failure("系统繁忙，请稍后再试~");
    }

}
