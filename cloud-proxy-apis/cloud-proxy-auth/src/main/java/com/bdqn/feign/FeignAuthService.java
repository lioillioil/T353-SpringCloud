package com.bdqn.feign;

import com.bdqn.base.R;
import com.bdqn.feign.fallback.FeignAuthServiceFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * FeignAuthService
 *
 * @author LILIBO
 * @since 2023-03-09
 */
@FeignClient(name = "cloud-center-auth", fallback = FeignAuthServiceFallback.class)
public interface FeignAuthService {

    /**
     * 远程调用 认证模块进行Token验证
     *
     * @param token 验证的Token
     * @return 验证是否成功
     */
    @PostMapping("/auth/verify")
    R verify(@RequestBody String token);

}
