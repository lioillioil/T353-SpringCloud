package com.bdqn.feign;

import com.bdqn.base.R;
import com.bdqn.feign.fallback.FeignPaymentServiceFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * FeignPaymentService 支付服务远程调用接口
 *
 * @author LILIBO
 * @since 2024-03-20
 */
@FeignClient(name = "cloud-service-payment", fallback = FeignPaymentServiceFallback.class)
public interface FeignPaymentService {

    /**
     * 根据订单编号完成支付
     *
     * @param orderId 订单编号
     * @param money 支付金额
     * @return
     */
    @PostMapping("/pay/byOrderId")
    R payByOrderId(@RequestParam("orderId") Long orderId, @RequestParam("money") Double money);

}
