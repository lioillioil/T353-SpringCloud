package com.bdqn.feign.fallback;

import com.bdqn.base.R;
import com.bdqn.feign.FeignPaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * FeignPaymentServiceFallback
 *
 * @author LILIBO
 * @since 2024-03-20
 */
@Slf4j
@Component
public class FeignPaymentServiceFallback implements FeignPaymentService {

    /**
     * 根据订单编号完成支付（服务降级）
     */
    @Override
    public R payByOrderId(Long orderId, Double money) {
        log.debug("[服务降级] payByOrderId --> orderId: {} money: {}", orderId, money);
        return R.failure("系统繁忙，请稍后再试~");
    }

}
