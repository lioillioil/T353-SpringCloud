package com.bdqn.feign.fallback;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bdqn.base.Q;
import com.bdqn.base.R;
import com.bdqn.feign.FeignOrderService;
import com.bdqn.pojo.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * FeignOrderServiceFallback
 *
 * @author LILIBO
 * @since 2024-03-20
 */
@Slf4j
@Component
public class FeignOrderServiceFallback implements FeignOrderService {

    /**
     * 创建订单（调用支付模块完成订单支付）
     *
     * @param money 订单金额
     */
    @Override
    public R genOrder(Double money) {
        log.debug("[服务降级] {}", money);
        return R.failure("系统繁忙，请稍后再试~");
    }

    /**
     * 新增订单记录（服务降级）
     *
     * @param order 订单
     * @return R 成功返回true
     */
    @Override
    public R add(Order order) {
        log.debug("[服务降级] {}", order);
        return R.failure("系统繁忙，请稍后再试~");
    }

    /**
     * 保存或更新订单记录（服务降级）
     *
     * @param order 订单（带主键ID表示更新，否则为新增）
     * @return R 成功返回保存后的完整对象
     */
    @Override
    public R save(Order order) {
        log.debug("[服务降级] {}", order);
        return R.failure("系统繁忙，请稍后再试~");
    }

    /**
     * 修改订单记录（服务降级）
     *
     * @param order 订单
     * @return R 成功返回true
     */
    @Override
    public R update(Order order) {
        log.debug("[服务降级] {}", order);
        return R.failure("系统繁忙，请稍后再试~");
    }

    /**
     * 通过id删除一条订单记录（服务降级）
     *
     * @param id
     * @return R 成功返回true
     */
    @Override
    public R deleteById(Long id) {
        log.debug("[服务降级] {}", id);
        return R.failure("系统繁忙，请稍后再试~");
    }

    /**
     * 通过多个id批量删除订单记录（服务降级）
     *
     * @param ids
     * @return R
     */
    @Override
    public R removeByIds(List<Long> ids) {
        log.debug("[服务降级] {}", ids);
        return R.failure("系统繁忙，请稍后再试~");
    }

    /**
     * 通过id查询单条订单记录（服务降级）
     *
     * @param id
     * @return R
     */
    @Override
    public R<Order> getById(Long id) {
        log.debug("[服务降级] {}", id);
        return R.failure("系统繁忙，请稍后再试~");
    }

    /**
     * 查询订单列表（服务降级）
     *
     * @param order 订单
     * @return R
     */
    @Override
    public R<List<Order>> getList(Order order) {
        log.debug("[服务降级] {}", order);
        return R.failure("系统繁忙，请稍后再试~");
    }

    /**
     * 分页查询订单（服务降级）
     *
     * @param pageNo 当前页
     * @param pageSize 每页条数
     * @param order 订单
     * @return
     */
    @Override
    public R<IPage<Order>> getPage(Long pageNo, Long pageSize, Order order) {
        log.debug("[服务降级] {} {} {}", pageNo, pageSize, order);
        return R.failure("系统繁忙，请稍后再试~");
    }

    /**
     * 分页查询订单列表（服务降级）
     *
     * @param pageNo 当前页
     * @param pageSize 每页条数
     * @param param 通用查询参数
     * @return 返回分页数据列表，其中count为数据总量
     */
    @Override
    public R<List<Order>> getPageList(Long pageNo, Long pageSize, Q param) {
        log.debug("[服务降级] {} {} {}", pageNo, pageSize, param);
        return R.failure("系统繁忙，请稍后再试~");
    }

}
