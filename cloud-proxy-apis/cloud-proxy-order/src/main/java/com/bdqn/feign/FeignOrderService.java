package com.bdqn.feign;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bdqn.base.Q;
import com.bdqn.base.R;
import com.bdqn.feign.fallback.FeignOrderServiceFallback;
import com.bdqn.logger.WebLogger;
import com.bdqn.pojo.Order;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * FeignOrderService 支付服务远程调用接口
 *
 * @author LILIBO
 * @since 2024-03-20
 */
@FeignClient(name = "cloud-service-order", fallback = FeignOrderServiceFallback.class)
public interface FeignOrderService {

    /**
     * 创建订单（调用支付模块完成订单支付）
     *
     * @param money 订单金额
     */
    @WebLogger("创建订单（调用支付模块完成订单支付）")
    @PostMapping("/order/gen")
    R genOrder(@RequestParam("money") Double money);

    /**
     * 新增订单记录
     *
     * @param order 订单
     * @return R 成功返回true
     */
    @ApiOperation("新增订单记录")
    @PostMapping("/add")
    R add(@RequestBody Order order);

    /**
     * 保存或更新订单记录
     *
     * @param order 订单（带主键ID表示更新，否则为新增）
     * @return R 成功返回保存后的完整对象
     */
    @ApiOperation("保存或更新订单记录")
    @PostMapping("/save")
    R save(@ApiParam("订单对象") @RequestBody Order order);

    /**
     * 修改订单记录
     *
     * @param order 订单
     * @return R 成功返回true
     */
    @ApiOperation("修改订单记录")
    @PostMapping("/update")
    R update(@ApiParam("订单对象") @RequestBody Order order);

    /**
     * 通过id删除一条订单记录
     *
     * @param id
     * @return R 成功返回true
     */
    @ApiOperation("通过id删除一条订单记录")
    @PostMapping("/delete")
    R deleteById(@ApiParam("订单id") Long id);

    /**
     * 通过多个id批量删除订单记录
     *
     * @param ids
     * @return R
     */
    @ApiOperation("通过多个id批量删除订单记录")
    @PostMapping("/remove")
    R removeByIds(@ApiParam("订单id集合") @RequestBody List<Long> ids);

    /**
     * 通过id查询单条订单记录
     *
     * @param id
     * @return R
     */
    @ApiOperation("通过id查询单条订单记录")
    @GetMapping("/{id}")
    R<Order> getById(@PathVariable Long id);

    /**
     * 查询订单列表
     *
     * @param order 订单
     * @return R
     */
    @ApiOperation("查询订单列表")
    @GetMapping("/list")
    R<List<Order>> getList(Order order);

    /**
     * 分页查询订单
     *
     * @param pageNo 当前页
     * @param pageSize 每页条数
     * @param order 订单
     * @return
     */
    @ApiOperation("分页查询订单")
    @GetMapping("/page")
    R<IPage<Order>> getPage(@RequestParam(required = false, defaultValue = "1") Long pageNo, @ApiParam("每页条数") @RequestParam(required = false, defaultValue = "10") Long pageSize, @ApiParam("查询的订单对象") Order order);

    /**
     * 分页查询订单列表
     *
     * @param pageNo 当前页
     * @param pageSize 每页条数
     * @param param 通用查询参数
     * @return 返回分页数据列表，其中count为数据总量
     */
    @ApiOperation("分页查询订单列表")
    @GetMapping("/pagelist")
    R<List<Order>> getPageList(@RequestParam(required = false, defaultValue = "1") Long pageNo, @RequestParam(required = false, defaultValue = "10") Long pageSize, Q param);

}
