package com.bdqn;

import com.bdqn.pojo.GenConfig;
import com.bdqn.service.GeneratorService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 使用测试类生成代码
 *
 * @author LILIBO
 * @since 2023-04-25
 */
@SpringBootTest
public class GeneratorCodeTest {

    /**
     * 配置文件
     */
    @Resource
    private GenConfig genConfig;

    /**
     * 代码生成服务
     */
    @Resource
    private GeneratorService generatorService;

    /**
     * 根据需要按模板批量生成模块的代码（当表结构有更新时用于小范围更新模块代码，三层模式中灵活配置需要生成的部分）
     */
    @Test
    public void generatorMyCode() {
        System.out.println("----- Generator My Code ------");

        // 生成代码使用的模板（可根据需要生成相应的目标文件）
        List<String> templates = new ArrayList<>();
        templates.add("template/Entity.java.vm"); // 实体类模板
        templates.add("template/Mapper.java.vm"); // MyBatis接口模板
        templates.add("template/Mapper.xml.vm"); // MyBatis映射模板
        templates.add("template/Service.java.vm"); // 业务层接口模板
        templates.add("template/ServiceImpl.java.vm"); // 业务层实现类模板
        templates.add("template/Controller.java.vm"); // 控制器模板

        // 生成的代码参数配置
        String projectPath = "D:/Works/ktjiaoyu/T353-SpringCloud/cloud-generator/cloud-generator-factory/cloud-order-serve"; // 代码生成路径
        genConfig.setModule("bdqn"); // 模块名（不能为空）
        genConfig.setAuthor("LILIBO"); // 作者（可为空）
        genConfig.setDate("2022-06-24"); // 时间（可为空）
        genConfig.setTablePrefix(""); // 表前缀（可为空）
        genConfig.setProjectPath(projectPath); // 项目路径（不能为空）

        // 依据以下表生成代码
        String[] tableNames = {"order"}; // {"sys_user","sys_role","sys_user_role"};
        Arrays.stream(tableNames).forEach(tableName -> {
            genConfig.setTableName(tableName);
            boolean ok = generatorService.generatorCodeLocal(genConfig, templates);
            System.out.println("\n" + genConfig.toString() + "\n\n" + (ok ? "SUCCESS" : "FAILING"));
        });
        assert true;
    }

    /**
     * 根据需要按全量模板批量生成模块的代码（当表结构有更新时用于小范围更新模板代码，生成完整三层模式代码）
     */
    @Test
    public void generatorBatchCode() {
        System.out.println("----- Generator Batch Code ------");
        String projectPath = "D:/Works/ktjiaoyu/T353-SpringCloud/cloud-generator/cloud-generator-factory/cloud-order-serve"; // 代码生成路径
        genConfig.setModule("bdqn"); // 模块名（不能为空）
        genConfig.setAuthor("LILIBO"); // 作者（可为空）
        genConfig.setDate("2023-02-18"); // 时间（可为空）
        genConfig.setTablePrefix(""); // 表前缀（可为空）
        genConfig.setProjectPath(projectPath); // 项目路径（不能为空）

        // 自动从数据库中获取所有表生成代码
        List<Map<String, Object>> tables = generatorService.queryAllTables();
        tables.forEach(map -> map.forEach((key, value) -> {
            if ("tableName".equals(key)) {
                genConfig.setTableName(String.valueOf(value));
                boolean ok = generatorService.generatorCodeLocal(genConfig);
                System.out.println("\n" + genConfig + "\n\n" + (ok ? "SUCCESS" : "FAILING"));
            }
        }));
        assert true;
    }

    /**
     * 批量生成多模块项目的子模块（代码方式配置项目细节）
     */
    @Test
    public void generatorMyProjectCode() {
        System.out.println("----- Generator My Project Code ------");
        String projectPath = "D:/Works/ktjiaoyu/T353-SpringCloud/cloud-generator/cloud-generator-factory/cloud-order-serve"; // 代码生成路径
        genConfig.setBasePackage("cloud-order-serve"); // 项目名（服务名，不能为空）
        genConfig.setArtifactId("cloud-order-serve"); // 应用名（不能为空）
        genConfig.setApplication("Order"); // 启动类名（不能为空）
        genConfig.setModule("bdqn"); // 模块名（不能为空）
        genConfig.setAuthor("LILIBO"); // 作者（可为空）
        genConfig.setDate("2023-04-25"); // 时间（可为空）
        genConfig.setTablePrefix(""); // 表前缀（可为空）
        genConfig.setProjectPath(projectPath); // 项目路径（不能为空）

        // 自动从数据库中获取所有表生成代码
        List<Map<String, Object>> tables = generatorService.queryAllTables();

        tables.forEach(map -> map.forEach((key, value) -> {
            if ("tableName".equals(key)) {
                genConfig.setTableName(String.valueOf(value));
                boolean ok = generatorService.generatorProjectModule(genConfig);
                System.out.println("\n" + genConfig + "\n\n" + (ok ? "SUCCESS" : "FAILING"));
            }
        }));
        assert true;
    }

    /**
     * 批量生成多模块项目的子模块（通过application.yml配置项目细节）
     */
    @Test
    public void generatorAllProjectCode() {
        System.out.println("----- Generator All Project Code ------");

        // 自动从数据库中获取所有表生成代码
        List<Map<String, Object>> tables = generatorService.queryAllTables();

        tables.forEach(map -> map.forEach((key, value) -> {
            if ("tableName".equals(key)) {
                genConfig.setTableName(String.valueOf(value));
                boolean ok = generatorService.generatorProjectModule(genConfig);
                System.out.println("\n" + genConfig + "\n\n" + (ok ? "SUCCESS" : "FAILING"));
            }
        }));
        assert true;
    }

}