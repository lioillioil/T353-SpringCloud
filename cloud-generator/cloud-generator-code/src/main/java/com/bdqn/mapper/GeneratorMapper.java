package com.bdqn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.pojo.GenConfig;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 代码生成器数据访问层接口
 *
 * @author LILIBO
 * @since 2023-04-25
 */
public interface GeneratorMapper extends BaseMapper<GenConfig> {

    /**
     * 查询数据库中所有表
     *
     * @param tableName
     * @return
     */
    List<Map<String, Object>> queryTables(@Param("tableName") String tableName);

    /**
     * 分页查询表格
     *
     * @param page
     * @param tableName
     * @return
     */
    IPage<List<Map<String, Object>>> queryList(Page page, @Param("tableName") String tableName);

    /**
     * 查询表信息
     *
     * @param tableName 表名称
     * @return
     */
    Map<String, String> queryTable(@Param("tableName") String tableName);

    /**
     * 查询表列信息
     *
     * @param tableName 表名称
     * @return
     */
    List<Map<String, String>> queryColumns(@Param("tableName") String tableName);

}
