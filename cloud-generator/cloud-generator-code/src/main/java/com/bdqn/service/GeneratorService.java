package com.bdqn.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bdqn.pojo.GenConfig;

import java.util.List;
import java.util.Map;

/**
 * 代码生成业务逻辑接口
 *
 * @author LILIBO
 * @since 2023-04-25
 */
public interface GeneratorService extends IService<GenConfig> {

    /**
     * 生成工程
     *
     * @param genConfig 工程参数
     * @return
     */
    boolean generatorProjectModule(GenConfig genConfig);

    /**
     * 生成工程
     *
     * @param genConfig 工程参数
     * @param templates 生成模板
     * @return
     */
    boolean generatorProjectModule(GenConfig genConfig, List<String> templates);

    /**
     * 生成代码
     *
     * @param genConfig 生成配置
     * @return
     */
    boolean generatorCodeLocal(GenConfig genConfig);

    /**
     * 生成代码
     *
     * @param genConfig 生成配置
     * @return
     */
    boolean generatorCodeLocal(GenConfig genConfig, List<String> templates);

    /**
     * 生成代码
     *
     * @param genConfig 生成配置
     * @return
     */
    byte[] generatorCode(GenConfig genConfig);

    /**
     * 查询数据库中的所有表
     *
     * @return
     */
    List<Map<String, Object>> queryAllTables();

    /**
     * 分页查询表
     *
     * @param tableName 表名
     * @return
     */
    IPage<List<Map<String, Object>>> queryPage(Page page, String tableName);

}
