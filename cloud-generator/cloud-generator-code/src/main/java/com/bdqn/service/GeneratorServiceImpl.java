package com.bdqn.service;

import cn.hutool.core.io.IoUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bdqn.mapper.GeneratorMapper;
import com.bdqn.pojo.GenConfig;
import com.bdqn.util.GeneratorUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

/**
 * 代码生成业务逻辑实现
 *
 * @author LILIBO
 * @since 2023-04-25
 */
@Service
public class GeneratorServiceImpl extends ServiceImpl<GeneratorMapper, GenConfig> implements GeneratorService {

    @Resource
    private GeneratorMapper generatorMapper;

    /**
     * 查询数据库中的所有表
     *
     * @return
     */
    @Override
    public List<Map<String, Object>> queryAllTables() {
        return generatorMapper.queryTables(null);
    }

    /**
     * 分页查询表
     *
     * @param tableName 查询条件
     * @return
     */
    @Override
    public IPage<List<Map<String, Object>>> queryPage(Page page, String tableName) {
        return generatorMapper.queryList(page, tableName);
    }

    /**
     * 生成代码
     *
     * @param genConfig 生成配置
     * @return
     */
    @Override
    public boolean generatorProjectModule(GenConfig genConfig, List<String> templates) {
        // 生成工程
        GeneratorUtil.generatorProjectModule(genConfig);
        // 查询表信息
        Map<String, String> table = queryTable(genConfig.getTableName());
        // 查询列信息
        List<Map<String, String>> columns = queryColumns(genConfig.getTableName());
        // 生成代码
        GeneratorUtil.generatorCodeLocal(genConfig, templates, table, columns);
        return true;
    }

    /**
     * 生成代码
     *
     * @param genConfig 生成配置
     * @return
     */
    @Override
    public boolean generatorProjectModule(GenConfig genConfig) {
        // 生成工程
        GeneratorUtil.generatorProjectModule(genConfig);
        // 查询表信息
        Map<String, String> table = queryTable(genConfig.getTableName());
        // 查询列信息
        List<Map<String, String>> columns = queryColumns(genConfig.getTableName());
        // 生成代码
        GeneratorUtil.generatorCodeLocal(genConfig, table, columns);
        return true;
    }

    /**
     * 生成代码
     *
     * @param genConfig 生成配置
     * @return
     */
    @Override
    public boolean generatorCodeLocal(GenConfig genConfig) {
        // 查询表信息
        Map<String, String> table = queryTable(genConfig.getTableName());
        // 查询列信息
        List<Map<String, String>> columns = queryColumns(genConfig.getTableName());
        // 生成代码
        GeneratorUtil.generatorCodeLocal(genConfig, table, columns);
        return true;
    }

    /**
     * 生成代码
     *
     * @param genConfig 生成配置
     * @param templates 生成配置
     * @return
     */
    @Override
    public boolean generatorCodeLocal(GenConfig genConfig, List<String> templates) {
        // 查询表信息
        Map<String, String> table = queryTable(genConfig.getTableName());
        // 查询列信息
        List<Map<String, String>> columns = queryColumns(genConfig.getTableName());
        // 生成代码
        GeneratorUtil.generatorCodeLocal(genConfig, templates, table, columns);
        return true;
    }

    /**
     * 生成代码
     *
     * @param genConfig 生成配置
     * @return
     */
    @Override
    public byte[] generatorCode(GenConfig genConfig) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);

        // 查询表信息
        Map<String, String> table = queryTable(genConfig.getTableName());
        // 查询列信息
        List<Map<String, String>> columns = queryColumns(genConfig.getTableName());
        // 生成代码
        GeneratorUtil.generatorCodeZip(genConfig, table, columns, zip);
        IoUtil.close(zip);
        return outputStream.toByteArray();
    }

    private Map<String, String> queryTable(String tableName) {
        return generatorMapper.queryTable(tableName);
    }

    private List<Map<String, String>> queryColumns(String tableName) {
        return generatorMapper.queryColumns(tableName);
    }

}
