package com.bdqn.util;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import com.bdqn.base.Global;
import com.bdqn.pojo.ColumnEntity;
import com.bdqn.pojo.GenConfig;
import com.bdqn.pojo.TableEntity;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 代码生成器工具类
 *
 * @author LILIBO
 * @since 2023-04-25
 */
@Slf4j
@UtilityClass
public class GeneratorUtil {

    private static final String POM_XML_VM = "pom.xml.vm";
    private static final String BOOTSTRAP_YML_VM = "bootstrap.yml.vm";
    private static final String LOGBACK_SPRING_XML_VM = "logback-spring.xml.vm";
    private static final String APPLICATION_JAVA_VM = "Application.java.vm";
    private static final String ENTITY_JAVA_VM = "Entity.java.vm";
    private static final String CONTROLLER_JAVA_VM = "Controller.java.vm";
    private static final String SERVICE_JAVA_VM = "Service.java.vm";
    private static final String SERVICE_IMPL_JAVA_VM = "ServiceImpl.java.vm";
    private static final String MAPPER_JAVA_VM = "Mapper.java.vm";
    private static final String MAPPER_XML_VM = "Mapper.xml.vm";
    private static final String MENU_SQL_VM = "menu.sql.vm";
    private static final String INDEX_VUE_VM = "index.vue.vm";
    private static final String API_JS_VM = "api.js.vm";
    private static final String CRUD_JS_VM = "crud.js.vm";
    private static final String LAY_API_JS_VM = "api-route.js.vm";
    private static final String LAY_ADD_HTML_VM = "lay-add.html.vm";
    private static final String LAY_EDIT_HTML_VM = "lay-edit.html.vm";
    private static final String LAY_LIST_HTML_VM = "lay-list.html.vm";

    /* @Value("${system.codegen.project}")
    private String S_ProjectName = "project";
    @Value("${system.codegen.applicationName}")
    private String S_ApplicationName = "application";
    @Value("${system.codegen.package}")
    private String S_Package = "com";
    @Value("${system.codegen.module}")
    private String S_ModuleName = "bdqn";
    @Value("${system.codegen.tablePrefix}")
    private String S_TablePrefix = "";
    @Value("${system.codegen.author}")
    private String S_Author = "lilibo"; */

    private Map<String, String> getJdbcType() {
        Map<String, String> jdbcType = new HashMap<>();
        jdbcType.put("tinyint", "TINYINT");
        jdbcType.put("smallint", "SMALLINT");
        jdbcType.put("mediumint", "INTEGER");
        jdbcType.put("int", "INTEGER");
        jdbcType.put("integer", "INTEGER");
        jdbcType.put("bigint", "BIGINT");
        jdbcType.put("float", "FLOAT");
        jdbcType.put("double", "DOUBLE");
        jdbcType.put("decimal", "DECIMAL");
        jdbcType.put("bit", "BIT");
        jdbcType.put("boolean", "BOOLEAN");
        jdbcType.put("char", "CHAR");
        jdbcType.put("varchar", "VARCHAR");
        jdbcType.put("tinytext", "VARCHAR");
        jdbcType.put("text", "VARCHAR");
        jdbcType.put("mediumtext", "VARCHAR");
        jdbcType.put("longtext", "VARCHAR");
        jdbcType.put("date", "DATE");
        jdbcType.put("datetime", "TIME");
        jdbcType.put("timestamp", "TIMESTAMP");
        jdbcType.put("blob", "BLOB");
        return jdbcType;
    }

    private Map<String, String> getJavaType() {
        Map<String, String> javaType = new HashMap<>();
        javaType.put("TINYINT", "Integer");
        javaType.put("SMALLINT", "Integer");
        javaType.put("INTEGER", "Integer");
        javaType.put("BIGINT", "Long");
        javaType.put("FLOAT", "Float");
        javaType.put("DOUBLE", "Double");
        javaType.put("DECIMAL", "BigDecimal");
        javaType.put("BIT", "Boolean");
        javaType.put("BOOLEAN", "Boolean");
        javaType.put("CHAR", "String");
        javaType.put("VARCHAR", "String");
        javaType.put("DATE", "String");
        javaType.put("TIME", "String");
        javaType.put("TIMESTAMP", "String");
        javaType.put("BLOB", "String");
        return javaType;
    }

    private List<String> getTemplates() {
        List<String> templates = new ArrayList<>();
        templates.add("template/Entity.java.vm");
        templates.add("template/Mapper.java.vm");
        templates.add("template/Mapper.xml.vm");
        templates.add("template/Service.java.vm");
        templates.add("template/ServiceImpl.java.vm");
        templates.add("template/Controller.java.vm");
        templates.add("template/api-route.js.vm");
        templates.add("template/lay-add.html.vm");
        templates.add("template/lay-edit.html.vm");
        templates.add("template/lay-list.html.vm");
        // templates.add("template/menu.sql.vm");
        // templates.add("template/index.vue.vm");
        // templates.add("template/api.js.vm");
        // templates.add("template/crud.js.vm");
        return templates;
    }

    /**
     * 创建模块工程
     */
    public void generatorProjectModule(GenConfig genConfig) {
        // 项目生成路径
        String projectPath = genConfig.getProjectPath();

        // 设置velocity资源加载器
        Properties prop = new Properties();
        prop.put("resource.loader.file.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        Velocity.init(prop);

        VelocityContext context = new VelocityContext(genConfig.getContext());

        // -- 生成pom.xml文件 --

        // 获取pom.xml文件模板
        String pomTemplate = "template/" + POM_XML_VM;
        // 渲染模板
        StringWriter swPom = new StringWriter();
        Template tplPom = Velocity.getTemplate(pomTemplate, CharsetUtil.UTF_8);
        tplPom.merge(context, swPom);

        // pom.xml文件写入路径
        String pomPath = projectPath + File.separator + "pom.xml";
        // 将生成的文件写到目录
        FileUtil.writeBytes(swPom.toString().getBytes(), pomPath);

        // -- 生成Application.java启动类 --
        // 获取Application.java文件模板
        String applicationTemplate = "template/" + APPLICATION_JAVA_VM;
        // 渲染模板
        StringWriter swApplication = new StringWriter();
        Template tplApplication = Velocity.getTemplate(applicationTemplate, CharsetUtil.UTF_8);
        tplApplication.merge(context, swApplication);

        // Application.java文件写入路径
        // src\main\java\com\bdqn\SpringBootApplication.java
        String applicationPath = projectPath + File.separator + "src/main/java" + File.separator + genConfig.getBasePackage() + File.separator + genConfig.getModule() + File.separator + genConfig.getApplication() + "Application.java";
        // 将生成的文件写到目录
        FileUtil.writeBytes(swApplication.toString().getBytes(), applicationPath);

        // -- 生成bootstrap.yml配置文件 --
        // 获取bootstrap.yml文件模板
        String bootstrapTemplate = "template/" + BOOTSTRAP_YML_VM;
        // 渲染模板
        StringWriter swBootstrap = new StringWriter();
        Template tplBootstrap = Velocity.getTemplate(bootstrapTemplate, CharsetUtil.UTF_8);
        tplBootstrap.merge(context, swBootstrap);

        // bootstrap.yml文件写入路径
        String bootstrapPath = projectPath + File.separator + "src/main/resources" + File.separator + "bootstrap.yml";
        // 将生成的文件写到目录
        FileUtil.writeBytes(swBootstrap.toString().getBytes(), bootstrapPath);

        // -- 生成logback-spring.xml配置文件 --
        // 获取logback-spring.xml文件模板
        String logbackTemplate = "template/" + LOGBACK_SPRING_XML_VM;
        // 渲染模板
        StringWriter swLogback = new StringWriter();
        Template tplLogback = Velocity.getTemplate(logbackTemplate, CharsetUtil.UTF_8);
        tplLogback.merge(context, swLogback);

        // logback-spring.xml文件写入路径
        String logbackPath = projectPath + File.separator + "src/main/resources" + File.separator + "logback-spring.xml";
        // 将生成的文件写到目录
        FileUtil.writeBytes(swLogback.toString().getBytes(), logbackPath);
    }

    /**
     * 获取模板上下文配置
     *
     * @param genConfig 公共配置
     * @param table 表名
     * @param columns 字段列表
     * @return 模板上下文配置对象
     */
    public VelocityContext getVelocityContext(GenConfig genConfig, Map<String, String> table, List<Map<String, String>> columns) {
        // 配置信息
        boolean hasBigDecimal = false;
        // 表信息
        TableEntity tableEntity = new TableEntity();
        tableEntity.setTableName(table.get("tableName"));

        if (StrUtil.isNotBlank(genConfig.getComments())) {
            tableEntity.setComments(genConfig.getComments());
        } else {
            String comments = table.get("tableComment");
            comments = comments.indexOf("表") == -1 ? comments : comments.substring(0, comments.indexOf("表"));
            tableEntity.setComments(comments);
        }

        // 获取表前缀配置
        String tablePrefix = genConfig.getTablePrefix();
        // 表名转换成Java类名
        String className = tableToJava(tableEntity.getTableName(), tablePrefix);
        tableEntity.setCaseClassName(className);
        tableEntity.setLowerClassName(StringUtils.uncapitalize(className));

        // 列信息
        List<ColumnEntity> columnList = new ArrayList<>();
        for (Map<String, String> column : columns) {
            ColumnEntity columnEntity = new ColumnEntity();
            columnEntity.setColumnName(column.get("columnName"));
            columnEntity.setJdbcType(getJdbcType().get(column.get("dataType")));
            columnEntity.setComments(column.get("columnComment"));
            columnEntity.setExtra(column.get("extra"));

            // 列名转换成Java属性名
            String attrName = columnToJava(columnEntity.getColumnName());
            columnEntity.setCaseAttrName(attrName);
            columnEntity.setLowerAttrName(StringUtils.uncapitalize(attrName));

            // 列的数据类型，转换成Java类型
            String javaType = getJavaType().get(columnEntity.getJdbcType());
            columnEntity.setJavaType(javaType);
            if (!hasBigDecimal && "BigDecimal".equals(javaType)) {
                hasBigDecimal = true;
            }
            // 是否主键
            if ("PRI".equalsIgnoreCase(column.get("columnKey")) && tableEntity.getPk() == null) {
                tableEntity.setPk(columnEntity);
            }
            columnList.add(columnEntity);
        }
        tableEntity.setColumns(columnList);

        // 没主键，则第一个字段为主键
        if (tableEntity.getPk() == null) {
            tableEntity.setPk(tableEntity.getColumns().get(0));
        }

        // 封装模板数据
        Map<String, Object> contextMap = genConfig.getContext(); // 在公共参数的基础上
        contextMap.put("tableName", tableEntity.getTableName());
        contextMap.put("pk", tableEntity.getPk());
        contextMap.put("className", tableEntity.getCaseClassName());
        contextMap.put("classname", tableEntity.getLowerClassName());
        contextMap.put("pathName", tableEntity.getLowerClassName().toLowerCase());
        contextMap.put("columns", tableEntity.getColumns());
        contextMap.put("hasBigDecimal", hasBigDecimal);
        contextMap.put("comments", tableEntity.getComments());

        return new VelocityContext(contextMap);
    }

    /**
     * 在本地生成代码（写磁盘）
     *
     * @param genConfig 公共配置
     * @param table 表名
     * @param columns 字段列表
     * @return 生成是否成功
     */
    public boolean generatorCodeLocal(GenConfig genConfig, Map<String, String> table, List<Map<String, String>> columns) {

        // 设置velocity资源加载器
        Properties prop = new Properties();
        prop.put("resource.loader.file.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        Velocity.init(prop);

        VelocityContext context = getVelocityContext(genConfig, table, columns);

        // 获取模板列表
        List<String> templates = getTemplates();
        for (String template : templates) {
            // 渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, CharsetUtil.UTF_8);
            tpl.merge(context, sw);
            // 将生成的文件写到目录
            String fileName = Objects.requireNonNull(getLocalFileName(genConfig.getProjectPath(), template, context.get("tableName").toString(), context.get("className").toString(), genConfig.getBasePackage(), genConfig.getModule()));
            FileUtil.writeBytes(sw.toString().getBytes(), fileName);
        }
        return true;
    }

    /**
     * 在本地生成代码（写磁盘）
     *
     * @param genConfig 公共配置
     * @param table 表名
     * @param columns 字段列表
     * @return 生成是否成功
     */
    public boolean generatorCodeLocal(GenConfig genConfig, List<String> templates, Map<String, String> table, List<Map<String, String>> columns) {

        // 设置velocity资源加载器
        Properties prop = new Properties();
        prop.put("resource.loader.file.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        Velocity.init(prop);

        VelocityContext context = getVelocityContext(genConfig, table, columns);

        // 获取模板列表
        if (templates != null) {
            for (String template : templates) {
                // 渲染模板
                StringWriter sw = new StringWriter();
                Template tpl = Velocity.getTemplate(template, CharsetUtil.UTF_8);
                tpl.merge(context, sw);
                // 将生成的文件写到目录
                String fileName = Objects.requireNonNull(getLocalFileName(genConfig.getProjectPath(), template, context.get("tableName").toString(), context.get("className").toString(), genConfig.getBasePackage(), genConfig.getModule()));
                FileUtil.writeBytes(sw.toString().getBytes(), fileName);
            }
        }
        return true;
    }

    /**
     * 生成代码并打压缩包（Zip包）
     *
     * @param genConfig 公共配置
     * @param table 表名
     * @param columns 字段列表
     * @param zip Zip对象
     */
    public boolean generatorCodeZip(GenConfig genConfig, Map<String, String> table, List<Map<String, String>> columns, ZipOutputStream zip) {

        // 设置velocity资源加载器
        Properties prop = new Properties();
        prop.put("resource.loader.file.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        Velocity.init(prop);

        VelocityContext context = getVelocityContext(genConfig, table, columns);

        // 获取模板列表
        List<String> templates = getTemplates();
        for (String template : templates) {
            // 渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, CharsetUtil.UTF_8);
            tpl.merge(context, sw);

            try {
                // 添加到zip
                zip.putNextEntry(new ZipEntry(Objects.requireNonNull(getFileName(template, context.get("tableName").toString(), context.get("className").toString(), context.get("package").toString(), context.get("module").toString()))));
                IoUtil.write(zip, CharsetUtil.UTF_8, false, sw.toString());
                IoUtil.close(sw);
                zip.closeEntry();
            } catch (IOException e) {
                log.debug("渲染模板失败，表名：" + table.get("tableName"), e);
            }
        }
        return true;
    }

    /**
     * 列名转换成Java属性名
     */
    private String columnToJava(String columnName) {
        return WordUtils.capitalizeFully(columnName, new char[]{'_'}).replace("_", "");
    }

    /**
     * 表名转换成Java类名
     */
    private String tableToJava(String tableName, String tablePrefix) {
        if (StringUtils.isNotBlank(tablePrefix)) {
            tableName = tableName.replace(tablePrefix, "");
        }
        return columnToJava(tableName);
    }

    /**
     * 获取文件名（通过IDE运行直接生成模块代码）
     */
    private String getLocalFileName(String template, String tableName, String className, String packageName, String moduleName) {
        return getLocalFileName(System.getProperty(Global.USER_DIR), template, tableName, className, packageName, moduleName);
    }

    /**
     * 获取文件名（通过IDE运行直接生成模块代码）
     */
    private String getLocalFileName(String projectPath, String template, String tableName, String className, String packageName, String moduleName) {
        projectPath = StringUtils.isBlank(projectPath) ? System.getProperty(Global.USER_DIR) : projectPath;
        String packagePath = projectPath + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator;
        String resourcesPath = projectPath + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator;
        if (StringUtils.isNotBlank(packageName)) {
            packagePath += packageName.replace(".", File.separator) + File.separator + moduleName + File.separator;
        }
        if (template.contains(ENTITY_JAVA_VM)) {
            return packagePath + "pojo" + File.separator + className + ".java";
        }
        if (template.contains(MAPPER_JAVA_VM)) {
            return packagePath + "mapper" + File.separator + className + "Mapper.java";
        }
        if (template.contains(SERVICE_JAVA_VM)) {
            return packagePath + "service" + File.separator + className + "Service.java";
        }
        if (template.contains(SERVICE_IMPL_JAVA_VM)) {
            return packagePath + "service" + File.separator + className + "ServiceImpl.java";
        }
        if (template.contains(CONTROLLER_JAVA_VM)) {
            return packagePath + "controller" + File.separator + className + "Controller.java";
        }
        if (template.contains(MAPPER_XML_VM)) {
            return resourcesPath + "mapper" + File.separator + className + "Mapper.xml";
        }
        if (template.contains(MENU_SQL_VM)) {
            return resourcesPath + Global.FRONT_PROJECT + File.separator + className.toLowerCase() + "_menu.sql";
        }
        if (template.contains(INDEX_VUE_VM)) {
            return resourcesPath + Global.FRONT_PROJECT + File.separator + "views" + File.separator + moduleName + File.separator + className.toLowerCase() + File.separator + "index.vue";
        }
        if (template.contains(API_JS_VM)) {
            return resourcesPath + Global.FRONT_PROJECT + File.separator + "api" + File.separator + moduleName + File.separator + className.toLowerCase() + ".js";
        }
        if (template.contains(CRUD_JS_VM)) {
            return resourcesPath + Global.FRONT_PROJECT + File.separator + "const" + File.separator + moduleName + File.separator + className.toLowerCase() + ".js";
        }
        if (template.contains(LAY_API_JS_VM)) {
            return resourcesPath + Global.FRONT_PROJECT + File.separator + "js" + File.separator + "api-route-" + className.toLowerCase() + ".js";
        }
        if (template.contains(LAY_ADD_HTML_VM)) {
            return resourcesPath + Global.FRONT_PROJECT + File.separator + "page" + File.separator + tableName + File.separator + "add.html";
        }
        if (template.contains(LAY_EDIT_HTML_VM)) {
            return resourcesPath + Global.FRONT_PROJECT + File.separator + "page" + File.separator + tableName + File.separator + "edit.html";
        }
        if (template.contains(LAY_LIST_HTML_VM)) {
            return resourcesPath + Global.FRONT_PROJECT + File.separator + "page" + File.separator + tableName + File.separator + "list.html";
        }

        return null;
    }

    /**
     * 获取文件名（通过Web端生成代码压缩包下载）
     */
    private String getFileName(String template, String tableName, String className, String packageName, String moduleName) {
        String packagePath = Global.BACK_PROJECT + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator;
        if (StringUtils.isNotBlank(packageName)) {
            packagePath += packageName.replace(".", File.separator) + File.separator + moduleName + File.separator;
        }
        if (template.contains(ENTITY_JAVA_VM)) {
            return packagePath + "pojo" + File.separator + className + ".java";
        }
        if (template.contains(MAPPER_JAVA_VM)) {
            return packagePath + "mapper" + File.separator + className + "Mapper.java";
        }
        if (template.contains(SERVICE_JAVA_VM)) {
            return packagePath + "service" + File.separator + className + "Service.java";
        }
        if (template.contains(SERVICE_IMPL_JAVA_VM)) {
            return packagePath + "service" + File.separator + className + "ServiceImpl.java";
        }
        if (template.contains(CONTROLLER_JAVA_VM)) {
            return packagePath + "controller" + File.separator + className + "Controller.java";
        }
        if (template.contains(MAPPER_XML_VM)) {
            return Global.BACK_PROJECT + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "mapper" + File.separator + className + "Mapper.xml";
        }
        if (template.contains(MENU_SQL_VM)) {
            return className.toLowerCase() + "_menu.sql";
        }
        if (template.contains(INDEX_VUE_VM)) {
            return Global.FRONT_PROJECT + File.separator + "src" + File.separator + "views" + File.separator + moduleName + File.separator + className.toLowerCase() + File.separator + "index.vue";
        }
        if (template.contains(API_JS_VM)) {
            return Global.FRONT_PROJECT + File.separator + "src" + File.separator + "api" + File.separator + moduleName + File.separator + className.toLowerCase() + ".js";
        }
        if (template.contains(CRUD_JS_VM)) {
            return Global.FRONT_PROJECT + File.separator + "src" + File.separator + "const" + File.separator + "crud" + File.separator + moduleName + File.separator + className.toLowerCase() + ".js";
        }
        if (template.contains(LAY_API_JS_VM)) {
            return Global.FRONT_PROJECT + File.separator + "js" + File.separator + "api-route-" + className.toLowerCase() + ".js";
        }
        if (template.contains(LAY_ADD_HTML_VM)) {
            return Global.FRONT_PROJECT + File.separator + "page" + File.separator + tableName + File.separator + "add.html";
        }
        if (template.contains(LAY_EDIT_HTML_VM)) {
            return Global.FRONT_PROJECT + File.separator + "page" + File.separator + tableName + File.separator + "edit.html";
        }
        if (template.contains(LAY_LIST_HTML_VM)) {
            return Global.FRONT_PROJECT + File.separator + "page" + File.separator + tableName + File.separator + "list.html";
        }
        return null;
    }

}
