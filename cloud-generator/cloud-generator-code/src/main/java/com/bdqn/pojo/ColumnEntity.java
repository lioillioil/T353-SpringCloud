package com.bdqn.pojo;

import io.swagger.annotations.Api;
import lombok.Data;

/**
 * 列属性
 *
 * @author LILIBO
 * @since 2023-04-25
 */
@Api("列属性")
@Data
public class ColumnEntity {

    /**
     * 列表
     */
    private String columnName;

    /**
     * JDBC数据类型
     */
    private String jdbcType;

    /**
     * JAVA数据类型
     */
    private String javaType;

    /**
     * 备注
     */
    private String comments;

    /**
     * 驼峰属性
     */
    private String caseAttrName;

    /**
     * 普通属性
     */
    private String lowerAttrName;

    /**
     * 其他信息
     */
    private String extra;

}
