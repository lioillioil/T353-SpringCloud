package com.bdqn.pojo;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 系统配置
 *
 * @author LILIBO
 * @since 2023-04-25
 */
@Api("系统配置")
@Data
@Component
public class GenConfig {

    /**
     * 生成到的项目路径
     */
    @Value("${system.codegen.projectPath}")
    private String projectPath = System.getProperty("user.dir");

    /**
     * 项目名称
     */
    @Value("${system.codegen.project}")
    private String project = "project";

    /**
     * 父级应用ID
     */
    @Value("${system.codegen.parentId}")
    private String parentId = "cloud-service-xxx";

    /**
     * 应用ID
     */
    @Value("${system.codegen.artifactId}")
    private String artifactId = "cloud-xxx-serve";

    /**
     * 应用程序入口文件名
     */
    @Value("${system.codegen.application}")
    private String application = "SpringBoot";

    /**
     * 包名
     */
    @Value("${system.codegen.basePackage}")
    private String basePackage = "com";

    /**
     * 模块名称
     */
    @Value("${system.codegen.module}")
    private String module = "bdqn";

    /**
     * 作者
     */
    @Value("${system.codegen.author}")
    private String author = "LILIBO";

    /**
     * 时间
     */
    @Value("${system.codegen.date}")
    private String date = "2021-01-01";

    /**
     * 表前缀
     */
    @Value("${system.codegen.tablePrefix}")
    private String tablePrefix = "";

    /**
     * 表名称
     */
    private String tableName;

    /**
     * 表备注
     */
    private String comments;

    /**
     * 获取封装的模板变量
     *
     * @return
     */
    public Map<String, Object> getContext() {
        Map<String, Object> map = new HashMap<>();
        map.put("project", this.project);
        map.put("parentId", this.parentId);
        map.put("artifactId", this.artifactId);
        map.put("application", this.application);
        map.put("basePackage", this.basePackage);
        map.put("module", this.module);
        map.put("author", this.author);
        map.put("date", StrUtil.isNotBlank(this.date) ? this.date : DateUtil.format(DateUtil.date(), "yyyy-MM-dd"));
        map.put("tablePrefix", this.tablePrefix);
        map.put("projectPath", this.projectPath);
        return map;
    }

}
