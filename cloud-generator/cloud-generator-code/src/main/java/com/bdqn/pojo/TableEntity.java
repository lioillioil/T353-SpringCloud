package com.bdqn.pojo;

import io.swagger.annotations.Api;
import lombok.Data;

import java.util.List;

/**
 * 表属性
 *
 * @author LILIBO
 * @since 2023-04-25
 */
@Api("表属性")
@Data
public class TableEntity {

    /**
     * 名称
     */
    private String tableName;

    /**
     * 备注
     */
    private String comments;

    /**
     * 主键
     */
    private ColumnEntity pk;

    /**
     * 列名
     */
    private List<ColumnEntity> columns;

    /**
     * 驼峰类型
     */
    private String caseClassName;

    /**
     * 普通类型
     */
    private String lowerClassName;

}
