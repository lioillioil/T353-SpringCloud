/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : 127.0.0.1:3306
 Source Schema         : demo

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 15/09/2021 09:06:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for grade
-- ----------------------------
DROP TABLE IF EXISTS `grade`;
CREATE TABLE `grade`  (
  `grade_id` int(4) UNSIGNED NOT NULL COMMENT '年级编号',
  `grade_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '年级名称'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '年级表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of grade
-- ----------------------------
INSERT INTO `grade` VALUES (1, 'S1');
INSERT INTO `grade` VALUES (2, 'S2');
INSERT INTO `grade` VALUES (3, 'S3');

-- ----------------------------
-- Table structure for result
-- ----------------------------
DROP TABLE IF EXISTS `result`;
CREATE TABLE `result`  (
  `student_no` int(4) NOT NULL COMMENT '学号',
  `subject_id` int(4) NOT NULL COMMENT '课程编号',
  `exam_date` datetime(0) NOT NULL COMMENT '考试日期',
  `exam_result` int(4) NOT NULL COMMENT '考试成绩',
  PRIMARY KEY (`student_no`, `subject_id`, `exam_date`) USING BTREE,
  INDEX `index_result`(`exam_result`) USING BTREE,
  CONSTRAINT `FK_student_result` FOREIGN KEY (`student_no`) REFERENCES `student` (`student_no`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '成绩表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of result
-- ----------------------------
INSERT INTO `result` VALUES (10007, 1, '2016-02-17 00:00:00', 23);
INSERT INTO `result` VALUES (10001, 1, '2016-02-17 00:00:00', 46);
INSERT INTO `result` VALUES (10000, 1, '2016-02-17 00:00:00', 60);
INSERT INTO `result` VALUES (10003, 1, '2016-02-17 00:00:00', 60);
INSERT INTO `result` VALUES (10004, 1, '2016-02-17 00:00:00', 60);
INSERT INTO `result` VALUES (10000, 1, '2016-02-15 00:00:00', 71);
INSERT INTO `result` VALUES (10002, 1, '2016-02-17 00:00:00', 83);
INSERT INTO `result` VALUES (10006, 1, '2016-02-17 00:00:00', 93);
INSERT INTO `result` VALUES (10005, 1, '2016-02-17 00:00:00', 95);
INSERT INTO `result` VALUES (10008, 2, '2020-01-01 00:00:00', 99);

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `student_no` int(4) NOT NULL COMMENT '学号',
  `login_pwd` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `student_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学生姓名',
  `sex` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '男' COMMENT '性别',
  `grade_id` int(4) UNSIGNED NULL DEFAULT NULL COMMENT '年级编号',
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '地址不详' COMMENT '地址',
  `born_date` datetime(0) NULL DEFAULT NULL COMMENT '出生时间',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮件账号',
  `identity_card` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号码',
  PRIMARY KEY (`student_no`) USING BTREE,
  INDEX `index_student_student_name`(`student_name`) USING BTREE,
  INDEX `index_student_student_name_grade_id`(`student_name`, `grade_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '学生表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (10000, '123', '曹嘉康', '男', 1, '13645667783', '天津市河西区', '1990-09-08 00:00:00', NULL, NULL);
INSERT INTO `student` VALUES (10001, '123', '李文才', '男', 1, '13645667890', '地址不详', '1994-04-12 00:00:00', NULL, NULL);
INSERT INTO `student` VALUES (10002, '123', '李斯文', '男', 1, '13645556793', '河南洛阳', '1993-07-23 00:00:00', NULL, NULL);
INSERT INTO `student` VALUES (10003, '123', '张萍', '女', 1, '13642345112', '地址不详', '1995-06-10 00:00:00', NULL, NULL);
INSERT INTO `student` VALUES (10004, '123', '韩秋洁', '女', 1, '13812344566', '北京市海淀区', '1995-07-15 00:00:00', NULL, NULL);
INSERT INTO `student` VALUES (10005, '123', '张秋丽', '女', 1, '13567893246', '北京市东城区', '1994-01-17 00:00:00', NULL, NULL);
INSERT INTO `student` VALUES (10006, '123', '肖梅', '女', 1, '13563456721', '河北省石家庄市', '1991-02-17 00:00:00', NULL, NULL);
INSERT INTO `student` VALUES (10007, '123', '泰洋', '男', 1, '13056434411', '上海市卢湾区', '1992-04-18 00:00:00', NULL, NULL);
INSERT INTO `student` VALUES (10008, '123', '何婧婧', '女', 1, '13054335221', '广州市天河区', '1997-07-23 00:00:00', NULL, NULL);
INSERT INTO `student` VALUES (20000, '000', '王宝宝', '男', 2, '15076552323', '地址不详', '1996-06-05 00:00:00', 'stu20000@163.com', NULL);
INSERT INTO `student` VALUES (20010, '123', '何小华', '女', 2, '13318877954', '地址不详', '1995-09-10 00:00:00', NULL, NULL);
INSERT INTO `student` VALUES (30011, '123', '陈志强', '女', 3, '13689965430', '地址不详', '1994-09-27 00:00:00', NULL, NULL);
INSERT INTO `student` VALUES (30012, '123', '李露露', '女', 3, '13685678854', '地址不详', '1992-09-27 00:00:00', NULL, NULL);
INSERT INTO `student` VALUES (30018, '123', '余昌霖', '男', 2, '13645667785', '天津市河西区', '1990-09-08 00:00:00', NULL, NULL);

-- ----------------------------
-- Table structure for subject
-- ----------------------------
DROP TABLE IF EXISTS `subject`;
CREATE TABLE `subject`  (
  `subject_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '课程编号',
  `subject_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '课程名称',
  `class_hour` int(4) NULL DEFAULT NULL COMMENT '学时',
  `grade_id` int(4) NULL DEFAULT NULL COMMENT '年级编号',
  PRIMARY KEY (`subject_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '科目表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of subject
-- ----------------------------
INSERT INTO `subject` VALUES (1, 'Logic Java', 210, 1);
INSERT INTO `subject` VALUES (2, 'HTML', 160, 1);
INSERT INTO `subject` VALUES (3, 'Java OOP', 230, 2);
INSERT INTO `subject` VALUES (4, 'Java实现数据库编程', 100, 2);

SET FOREIGN_KEY_CHECKS = 1;
