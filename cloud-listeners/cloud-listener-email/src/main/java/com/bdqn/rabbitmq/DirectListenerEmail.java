package com.bdqn.rabbitmq;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bdqn.base.Email;
import com.bdqn.base.Global;
import com.bdqn.service.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * DirectListenerEmail
 *
 * @author LILIBO
 * @since 2024/4/15
 */
@Slf4j
@Component
public class DirectListenerEmail {

    @Resource
    private EmailService emailService;

    /**
     * 监听RabbitMQ队列的消息（队列名：queue.cloud.email）
     *
     * @param json
     */
    @RabbitListener(queues = Global.QUEUE_NAME_EMAIL)
    public void listenerMQEmail(JSON json) {
        // 将Json格式的消息转换为对象
        Email email = JSONObject.toJavaObject(json, Email.class);
        // 发送邮件
        boolean result = emailService.sendEmail(email);
        log.debug("{} : {} -> {}", Global.QUEUE_NAME_EMAIL, email, result);
    }

}
