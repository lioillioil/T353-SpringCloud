package com.bdqn.service;

import com.bdqn.base.Email;

/**
 * EmailService
 *
 * @author LILIBO
 * @since 2023-03-11
 */
public interface EmailService {

    /**
     * 发送邮件
     *
     * @param email
     * @return
     */
    boolean sendEmail(Email email);

}
