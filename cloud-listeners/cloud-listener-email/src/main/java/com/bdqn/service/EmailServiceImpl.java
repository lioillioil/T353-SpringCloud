package com.bdqn.service;

import com.bdqn.base.Email;
import com.bdqn.util.EMailUtil;
import org.springframework.stereotype.Service;

/**
 * EmailServiceImpl
 *
 * @author LILIBO
 * @since 2023-03-11
 */
@Service
public class EmailServiceImpl implements EmailService {

    /**
     * 发送邮件
     *
     * @return
     */
    @Override
    public boolean sendEmail(Email email) {
        EMailUtil.send(email.getTo(), "订单支付成功", email.getContent());
        System.out.println("邮件发送成功 --------> " + email);
        return true;
    }

}
